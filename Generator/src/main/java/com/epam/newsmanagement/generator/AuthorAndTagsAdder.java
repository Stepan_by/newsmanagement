package com.epam.newsmanagement.generator;

import com.epam.newsmanagement.domain.Author;
import com.epam.newsmanagement.domain.News;
import com.epam.newsmanagement.domain.Tag;

import java.util.*;

public class AuthorAndTagsAdder {

    private static final int MAX_AUTHORS_FOR_NEWS = 10;
    private static final int MAX_TAGS_FOR_NEWS = 10;

    public List<News> generateAuthorsForNews(List<Author> authors, List<News> news) {
        Random random = new Random();
        for (News news1 : news) {
            int numberOfAuthors = random.nextInt(Math.min(MAX_AUTHORS_FOR_NEWS, authors.size()));
            List<Author> fakeAuthors = new ArrayList<>(authors);
            Set<Author> authorsForNews = new HashSet<>();
            while (numberOfAuthors -- > 0) {
                int s = random.nextInt(fakeAuthors.size() - 1) + 1;
                authorsForNews.add(fakeAuthors.remove(s));
            }
            news1.setAuthors(authorsForNews);
        }
        return news;
    }

    public List<News> generateTagsForNews(List<Tag> tags, List<News> news) {
        Random random = new Random();
        for (News news1 : news) {
            int numberOfTags = random.nextInt(MAX_TAGS_FOR_NEWS);
            List<Tag> fakeTags = new ArrayList<>(tags);
            Set<Tag> tagsForNews = new HashSet<>();
            while (numberOfTags -- > 0) {
                int s = fakeTags.size();
                tagsForNews.add(fakeTags.remove(random.nextInt(s)));
            }
            news1.setTags(tagsForNews);
        }
        return news;
    }
}
