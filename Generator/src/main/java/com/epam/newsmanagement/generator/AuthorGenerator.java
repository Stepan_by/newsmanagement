package com.epam.newsmanagement.generator;

import com.epam.newsmanagement.domain.Author;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.Scanner;
import java.util.regex.Pattern;

public class AuthorGenerator {

    public static final String FILE_NAME_PATH = "/girls.txt";
    public static final String FILE_SURNAME_PATH = "/Surnames.txt";
    private int namesLength;
    private int surnamesLength;

    public List<Author> generateAuthors(int number) {
        List<Author> authors = new ArrayList<>();
        try(Scanner nameScanner = new Scanner(getClass().getResourceAsStream(FILE_NAME_PATH));
            Scanner surnameScanner = new Scanner(getClass().getResourceAsStream(FILE_SURNAME_PATH))) {
            List<String> names = readNames(nameScanner);
            List<String> surnames = readNames(surnameScanner);
            namesLength = names.size();
            surnamesLength = surnames.size();
            while (number -- > 0) {
                int name = new Random().nextInt(namesLength);
                int surname = new Random().nextInt(surnamesLength);
                authors.add(new Author(names.get(name) + " " + surnames.get(surname)));
            }
        }
        return authors;
    }

    private List<String> readNames(Scanner scanner) {
        List<String> names = new ArrayList<>();
        while (scanner.hasNext()) {
            scanner.useDelimiter(Pattern.compile(",\r\n"));
            names.add(scanner.next());
        }
        return names;
    }

}
