package com.epam.newsmanagement.generator;

import com.epam.newsmanagement.domain.Comment;
import com.epam.newsmanagement.domain.News;

import java.sql.Date;
import java.util.*;

public class NewsCommentGenerator {

    public static final String TEXT_PATH = "/text.txt";
    private List<News> news = new ArrayList<>();
    private List<String> text = new ArrayList<>();

    public List<News> getNews() {
        return news;
    }

    public void setNews(List<News> news) {
        this.news = news;
    }

    public List<String> getText() {
        return text;
    }

    public void setText(List<String> text) {
        this.text = text;
    }

    public List<News> createNews(int number) {
        Random random = new Random();
        List<String> text = readText();
        while (number -- > 0) {
            news.add(new News(null,
                    createText(text, random, 10, 28),
                    createText(text, random, 60, 80),
                    createText(text, random, 400, 800),
                    new Date(Calendar.getInstance().getTimeInMillis())));
        }
        return news;
    }

    public List<Comment> createComments(Random random, int numberOfComments, List<News> newsInDb) {
        List<Comment> comments = new ArrayList<>();
        while (numberOfComments -- > 0) {
            comments.add(new Comment(newsInDb.get(random.nextInt(newsInDb.size())),
                    createText(text, random, 30, 80),
                    new Date(Calendar.getInstance().getTimeInMillis())));
        }
        return comments;
    }

    public List<String> readText() {
        try(Scanner scanner = new Scanner(getClass().getResourceAsStream(TEXT_PATH))) {
            while (scanner.hasNext()) {
                text.add(scanner.next());
            }
        }
        return text;
    }

    public String createText(List<String> text, Random random, int lowerBound, int upperBound) {
        StringBuilder builder = new StringBuilder();
        int rand = random.nextInt(upperBound - lowerBound) + lowerBound;
        while (builder.toString().length() < rand) {
            builder.append(text.get(random.nextInt(text.size()))).append(" ");
        }
        builder.delete(rand, builder.toString().length());
        return builder.toString();
    }
}
