package com.epam.newsmanagement;

import com.epam.newsmanagement.config.DatabaseContext;
import com.epam.newsmanagement.domain.Author;
import com.epam.newsmanagement.domain.Comment;
import com.epam.newsmanagement.domain.News;
import com.epam.newsmanagement.dto.SearchCriteria;
import com.epam.newsmanagement.service.exception.ServiceException;
import com.epam.newsmanagement.generator.AuthorAndTagsAdder;
import com.epam.newsmanagement.generator.AuthorGenerator;
import com.epam.newsmanagement.generator.NewsCommentGenerator;
import com.epam.newsmanagement.service.AuthorService;
import com.epam.newsmanagement.service.CommentService;
import com.epam.newsmanagement.service.NewsService;
import com.epam.newsmanagement.service.TagService;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class Main {

    public static final int AUTHORS_SIZE = 90;
    public static final int NEWS_SIZE = 10000;
    private static List<Author> authors = new ArrayList<>();
    private static List<News> news = new ArrayList<>();

    public static void main(String[] args) {
        AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(DatabaseContext.class);
        NewsCommentGenerator newsCommentGenerator = new NewsCommentGenerator();
        AuthorService authorService = context.getBean(AuthorService.class);
        NewsService newsService = context.getBean(NewsService.class);
        CommentService commentService = context.getBean(CommentService.class);
        TagService tagService = context.getBean(TagService.class);
        for (Author author : new AuthorGenerator().generateAuthors(AUTHORS_SIZE)) {
            try {
                authors.add(authorService.add(author));
            } catch (ServiceException e) {
                e.printStackTrace();
            }
        }
        for (News news1 : newsCommentGenerator.createNews(NEWS_SIZE)) {
            try {
                news.add(newsService.add(news1));
            } catch (ServiceException e) {
                e.printStackTrace();
            }
        }
        try {
            SearchCriteria searchCriteria = new SearchCriteria(0L, newsService.countNews(), null, null);
            for (Comment comment : newsCommentGenerator.createComments(
                    new Random(), 100000, newsService.findNewsAtPage(searchCriteria).getCollection())) {
                try {
                    commentService.add(comment);
                } catch (ServiceException e) {
                    e.printStackTrace();
                }
            }
        } catch (ServiceException e) {
            e.printStackTrace();
        }
        List<News> a = null;
        try {
            a = new AuthorAndTagsAdder().generateAuthorsForNews(authorService.findAllAuthors(), news);
        } catch (ServiceException e) {
            e.printStackTrace();
        }
        List<News> b = new ArrayList<>();
        try {
            b = new AuthorAndTagsAdder().generateTagsForNews(tagService.findAllTags(), a);
        } catch (ServiceException e) {
            e.printStackTrace();
        }
        for (News news1 : b) {
            try {
                newsService.update(news1);
            } catch (ServiceException e) {
                e.printStackTrace();
            }
        }
    }
}
