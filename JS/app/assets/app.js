angular.module('news-client', ['ui.router', 'ui.bootstrap'])
  .constant({
      SERVER_HOST: 'http://localhost:8082',
      NEWS_PER_PAGE : 10,
      COMMENTS_PER_PAGE : 10
    })
 .config(["$stateProvider", "$urlRouterProvider", "$locationProvider", function($stateProvider, $urlRouterProvider, $locationProvider) {

    $stateProvider.state("init", {
      abstract: true,
      views: {
        "header": {
          templateUrl: "assets/parts/header/header.html",
          controller: "HeaderCtrl"
        },
        "footer": {
          templateUrl: "assets/parts/footer/footer.html"
        },
        "menu": {
          templateUrl: "assets/parts/menu/menu.html",
          controller: "MenuCtrl"
        }
      }
    }).state("default", {
      parent: "init",
      url: "/",
      cache: false,
      views: {
        "content@": {
          templateUrl: "assets/parts/news/news.html",
          controller: "NewsCtrl"
        }
      }
    }).state("news", {
      parent: "init",
      url: "/news",
      abstract: true
    });

    $urlRouterProvider.otherwise("/");

    $locationProvider.html5Mode(true);

  }]).run(['$window', '$rootScope', "$templateCache", function ($window ,  $rootScope, $templateCache) {

      $rootScope.goBack = function(){
        $window.history.back();
      };

      $rootScope.tags = [];
      $rootScope.authors = [];
      $rootScope.top = null;

  }]);
