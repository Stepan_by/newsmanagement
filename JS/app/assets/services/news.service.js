angular.module('news-client').factory('NewsFactory', ['$http', 'SERVER_HOST', function($http, SERVER_HOST) {

  function getNewsInRange(from, number, tags, authors) {
    return $http.get(SERVER_HOST + "/api/news", {
      params: {
        from : from,
        number : number,
        tags : tags,
        authors : authors
      }
    });
  }

  function getNews(newsId) {
    return $http.get(SERVER_HOST + "/api/news/" + newsId);
  }

  function getTopNews(from, number, top) {
    return $http.get(SERVER_HOST + "/api/news/top" + top, {
      params : {
        from : from,
        number : number
      }
    });
  }

  function saveNews(news) {
    return $http.post(SERVER_HOST + "/api/news", news);
  }

  function deleteNews(newsId) {
    return $http.delete(SERVER_HOST + "/api/news/" + newsId);
  }

  function updateNews(news) {
    return $http.put(SERVER_HOST + "/api/news", news);
  }

  return {
    getNewsInRange: getNewsInRange,
    saveNews: saveNews,
    deleteNews : deleteNews,
    updateNews : updateNews,
    getNews : getNews,
    getTopNews : getTopNews
  };
}]);
