angular.module("news-client").factory("AuthorsFactory", ["$http", "SERVER_HOST", function($http, SERVER_HOST) {

  function getAllAuthors () {
    return $http.get(SERVER_HOST + "/api/authors");
  }

  function addAuthor (author) {
    return $http.post(SERVER_HOST + "/api/authors", author);
  }

  function deleteAuthor (author) {
    return $http.delete(SERVER_HOST + "/api/authors/" + author.id);
  }

  return {
    getAllAuthors : getAllAuthors,
    addAuthor : addAuthor,
    deleteAuthor : deleteAuthor
  };
}]);
