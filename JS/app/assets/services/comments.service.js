angular.module("news-client").factory("CommentsFactory", ["$http", "SERVER_HOST", function($http, SERVER_HOST) {

    function getCommentsForNews (newsId, from, number) {
      return $http.get(SERVER_HOST + "/api/news/" + newsId + "/comments", {
        params : {
          from : from,
          number : number
        }
      });
    }

    function addComment (newsId, comment) {
      return $http.post(SERVER_HOST + "/api/news/" + newsId + "/comments", comment);
    }

    function deleteComment (newsId, commentId) {
      return $http.delete(SERVER_HOST + "/api/news/" + newsId + "/comments/" + commentId);
    }

    return {
      addComment : addComment,
      getComments : getCommentsForNews,
      deleteComment : deleteComment
    };

}]);
