angular.module("news-client").factory("TagsFactory", ["$http", "SERVER_HOST", function($http, SERVER_HOST) {

  function getAllTags() {
    return $http.get(SERVER_HOST + "/api/tags");
  }

  function addTag(tag) {
    return $http.post(SERVER_HOST + "/api/tags", tag);
  }

  function deleteTag(tag) {
    return $http.delete(SERVER_HOST + "/api/tags/" + tag.name);
  }

  return {
    getAllTags : getAllTags,
    addTag : addTag,
    deleteTag : deleteTag
  };
}]);
