angular.module("news-client").factory("MathFactory", function () {

  function getSetDifference(array1, array2) {
    var a = [];
    var t = 0;
    for (i = 0; i < array1.length; i++) {
      for (j = 0; j < array2.length; j++) {
        if (array1[i].name == array2[j].name) {
          t = 1;
        }
      }
      if (t === 0) {
        a.push(array1[i]);
      }
      t = 0;
    }
    return a;
  }

  return {
    getSetDifference : getSetDifference
  };
});
