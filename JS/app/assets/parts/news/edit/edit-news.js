angular.module("news-client").config(["$stateProvider", function($stateProvider) {
  $stateProvider.state("news.edit", {
    url: "/edit/:id",
    views : {
      "content@": {
        templateUrl: "assets/parts/news/edit/edit-news.html",
        controller: "EditNewsCtrl"
      }
    }
  });
}]);
