angular.module("news-client").controller("EditNewsCtrl", ["$scope", "$state", "$uibModal", "$stateParams", "NewsFactory", "AuthorsFactory", "TagsFactory", "MathFactory", "$window",
        function($scope, $state, $uibModal, $stateParams, NewsFactory, AuthorsFactory, TagsFactory, MathFactory, $window) {

  NewsFactory.getNews($stateParams.id).then(function(result) {
    $scope.news = result.data;
  }, function (result) {
    console.log("Can't get news");
    $window.alert("Can't get news");
    $state.go("init.error", {code : result.status, text : "An error while getting news."});
  });

  $scope.update = function (event) {
    event.preventDefault();
    NewsFactory.updateNews($scope.news).then(function() {
      $state.go("default");
    }, function (result) {
      console.log("Can't update news");
      $window.alert("Can't update news");
      // $state.go("init.error", {code : result.status, text : "An error while updating news."});
    });
  };

  $scope.cancel = function(event) {
    event.preventDefault();
    $state.go("default");
  };

  $scope.openAuthorsModal = function (event, size) {
    event.preventDefault();
    var authorModal = $uibModal.open({
      templateUrl: "assets/parts/news/modal/authors-modal.html",
      controller: "AuthorsCtrl",
      size: size,
      resolve: {
        authors: function() {
          return AuthorsFactory.getAllAuthors().then(function(data) {
              return MathFactory.getSetDifference(data.data, $scope.news.authors);
          }, function (result) {
            console.log("Can't load all authors");
            $window.alert("Can't load all authors");
            $state.go("init.error", {code : result.status, text : "An error while loading all authors."});
          });
        }
      }
    });
    authorModal.result.then(function (authors) {
      $scope.news.authors = $scope.news.authors.concat(authors);
    });
  };

  $scope.openTagsModal = function (event, size) {
    event.preventDefault();
    var tagsModal = $uibModal.open({
      templateUrl: "assets/parts/news/modal/tags-modal.html",
      controller: "TagsCtrl",
      size: size,
      resolve: {
        tags: function() {
          return TagsFactory.getAllTags().then(function(data) {
              return MathFactory.getSetDifference(data.data, $scope.news.tags);
          }, function (result) {
            console.log("Can't load all tags");
            $window.alert("Can't load all tags");
            $state.go("init.error", {code : result.status, text : "An error while loading tags."});
          });
        }
      }
    });
    tagsModal.result.then(function (tags) {
      $scope.news.tags = $scope.news.tags.concat(tags);
    });
  };

  $scope.removeAuthor = function (author) {
    $scope.news.authors.splice($scope.news.authors.indexOf(author), 1);
  };

  $scope.removeTag = function (tag) {
    $scope.news.tags.splice($scope.news.tags.indexOf(tag), 1);
  };

  $scope.removeNews = function (newsId) {
    if (confirm("Are you sure?")) {
      NewsFactory.deleteNews(newsId).then(function () {
        $state.go("default");
      }, function (result) {
        console.log("Can't delete news");
        $window.alert("Can't delete news");
        $state.go("init.error", {code : result.status, text : "An error while deleting news."});
      });
    }
  };

}]);
