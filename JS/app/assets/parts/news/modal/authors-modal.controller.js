angular.module("news-client").controller("AuthorsCtrl", ["$scope", "$uibModalInstance", "authors", function($scope, $uibModalInstance, authors) {

  $scope.authors = authors;
  $scope.selected = [];

  $scope.ok = function () {
     $uibModalInstance.close($scope.selected);
   };

   $scope.cancel = function () {
     $uibModalInstance.dismiss();
   };
}]);
