angular.module("news-client").controller("TagsCtrl", ["$scope", "$uibModalInstance", "tags", function($scope, $uibModalInstance, tags) {

  $scope.tags = tags;
  $scope.selected = [];

  $scope.ok = function () {
     $uibModalInstance.close($scope.selected);
   };

   $scope.cancel = function () {
     $uibModalInstance.dismiss();
   };
}]);
