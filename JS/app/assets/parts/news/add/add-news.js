angular.module("news-client").config(["$stateProvider", function($stateProvider) {
  $stateProvider.state("news.add", {
    url: "/add",
    views : {
      "content@": {
        templateUrl: "assets/parts/news/add/add-news.html",
        controller: "AddNewsCtrl"
      }
    }
  });
}]);
