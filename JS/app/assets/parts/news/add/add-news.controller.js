angular.module("news-client").controller("AddNewsCtrl",
["$scope", "$state", "$uibModal", "NewsFactory", "TagsFactory", "AuthorsFactory", "MathFactory", "$window",
        function($scope, $state, $uibModal, NewsFactory, TagsFactory, AuthorsFactory, MathFactory, $window) {

  $scope.news = {
    title: "",
    shortText: "",
    fullText: "",
    authors : [],
    tags : []
  };

  $scope.save = function () {
    NewsFactory.saveNews($scope.news).then(function() {
      $state.go("default");
    }, function () {
      console.log("Can't save news");
      $window.alert("Can't save news");
    });
  };

  $scope.cancel = function (event) {
    event.preventDefault();
    $state.go("default");
  };

  $scope.openAuthorsModal = function (event, size) {
    event.preventDefault();
    var authorModal = $uibModal.open({
      templateUrl: "assets/parts/news/modal/authors-modal.html",
      controller: "AuthorsCtrl",
      size: size,
      resolve: {
        authors: function() {
          return AuthorsFactory.getAllAuthors().then(function(data) {
              return MathFactory.getSetDifference(data.data, $scope.news.authors);
          }, function (result) {
            console.log("Can't load all authors");
            $window.alert("Can't load all authors");
            $state.go("init.error", {code : result.status, text : "An error while loading all authors."});
          });
        }
      }
    });
    authorModal.result.then(function (authors) {
      $scope.news.authors = $scope.news.authors.concat(authors);
    }, function (data) {
      console.log("Can't send data from modal authors to add news view");
      $window.alert("Can't send data from modal authors to add news view");
    });
  };

  $scope.openTagsModal = function (event, size) {
    event.preventDefault();
    var tagsModal = $uibModal.open({
      templateUrl: "assets/parts/news/modal/tags-modal.html",
      controller: "TagsCtrl",
      size: size,
      resolve: {
        tags: function() {
          return TagsFactory.getAllTags().then(function(data) {
              return MathFactory.getSetDifference(data.data, $scope.news.tags);
          }, function (result) {
            console.log("Can't load all tags");
            $window.alert("Can't load all tags");
            $state.go("init.error", {code : result.status, text : "An error while loading all tags."});
          });
        }
      }
    });
    tagsModal.result.then(function (tags) {
      $scope.news.tags = $scope.news.tags.concat(tags);
    }, function (result) {
      console.log("Can't send data from modal tags to add tags view");
      $window.alert("Can't send data from modal tags to add tags view");
    });
  };

  $scope.removeAuthor = function (author) {
    $scope.news.authors.splice($scope.news.authors.indexOf(author), 1);
  };

  $scope.removeTag = function (tag) {
    $scope.news.tags.splice($scope.news.tags.indexOf(tag), 1);
  };

}]);
