angular.module("news-client").controller("NewsDetailsCtrl", ["$scope", "$state", "$stateParams", "CommentsFactory", "NewsFactory", "COMMENTS_PER_PAGE", "$window",
function($scope, $state, $stateParams, CommentsFactory, NewsFactory, COMMENTS_PER_PAGE, $window) {

  function loadNews(callback) {
    NewsFactory.getNews($stateParams.id).then(function(result) {
      $scope.news = result.data;
      callback();
    }, function (result) {
      console.log("Can't load news");
      $window.alert("Can't load news");
      $state.go("init.error", {code : result.status, text : "An error while loading news."});
    });
  }

  function loadComments(page) {
    CommentsFactory.getComments($scope.news.id, page * COMMENTS_PER_PAGE, COMMENTS_PER_PAGE).then(function (response) {
      $scope.pageComments = response.data;
      $scope.totalItems = response.headers("size");
    }, function (result) {
      console.log("Can't load comments");
      $window.alert("Can't load comments");
      $state.go("init.error", {code : result.status, text : "An error while loading comments."});
    });
  }

  $scope.comment = "";
  $scope.page = 1;
  $scope.maxSize = 3;
  $scope.itemsPerPage = COMMENTS_PER_PAGE;
  $scope.pageComments = [];
  $scope.changeComments = function (page) {
    $scope.pageComments = loadComments(page);
  };

  $scope.deleteComment = function(comment) {
    if (confirm("Are you sure?")) {
      CommentsFactory.deleteComment($scope.news.id, comment.id).then(function() {
        loadComments($scope.page - 1);
      }, function (result) {
        $window.alert("There was an exception while deleting comment " + comment.id);
        console.log("Can't delete comment " + commentId);
      });
    }
  };

  $scope.add = function() {
    var comment = {
      text : $scope.comment,
      creationDate : Date.now()
    };
    if (comment && comment.text.length > 0) {
      CommentsFactory.addComment($scope.news.id, comment).then(function(data) {
        loadComments(0);
      }, function (result) {
        console.log("Can't add comment");
        $window.alert("Can't add comment");
      });
      $scope.comment = "";
    }
  };

  loadNews(function() {
    loadComments(0);
  });

}]);
