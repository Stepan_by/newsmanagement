angular.module("news-client").config(["$stateProvider", function($stateProvider) {
  $stateProvider.state("news.details", {
    url: "/:id",
    views: {
      "content@": {
        templateUrl: "assets/parts/news/details/news-details.html",
        controller: "NewsDetailsCtrl"
      }
    }
  });
}]);
