angular.module("news-client").config(["$stateProvider", function ($stateProvider) {

  $stateProvider.state("news.commented", {
    url: "/top:top",
    views: {
      "content@": {
        templateUrl: "assets/parts/news/news.html",
        controller: "CommentedNewsCtrl"
      }
    }
  });
}]);
