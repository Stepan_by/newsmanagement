angular.module('news-client').controller("CommentedNewsCtrl", ['$scope', "$state", 'NewsFactory', "NEWS_PER_PAGE", "$stateParams", "$window", "$rootScope",
  function($scope, $state, NewsFactory, NEWS_PER_PAGE, $stateParams, $window, $rootScope) {

    $scope.page = 1;
    $scope.maxSize = 3;
    $scope.itemsPerPage = NEWS_PER_PAGE;

    function getTopNews (page) {
      NewsFactory.getTopNews(page * NEWS_PER_PAGE, NEWS_PER_PAGE, $stateParams.top).then(function (data) {
        $scope.news = data.data;
        $scope.totalItems = $stateParams.top;
      }, function (result) {
        console.log("Can't get top news");
        $window.alert("Can't get top news");
        $state.go("init.error", {code : result.status, text : "An error while getting top news."});
      });
    }

    $scope.loadNews = function (page) {
      getTopNews(page);
    };

    $scope.loadNews(0);

}]);
