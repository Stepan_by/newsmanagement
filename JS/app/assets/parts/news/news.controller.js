angular.module('news-client').controller('NewsCtrl',
['$scope', "$state", 'NewsFactory', "NEWS_PER_PAGE", "$window", "$rootScope",
function($scope, $state, NewsFactory, NEWS_PER_PAGE, $window, $rootScope) {

  $scope.loadNews = function (page) {
    NewsFactory.getNewsInRange(NEWS_PER_PAGE * page, NEWS_PER_PAGE,
      $scope.tags.map(function (element) {
        return element.name;
      }),
      $scope.authors.map(function (element) {
        return element.id;
      })
    ).then(function(response) {
      $scope.news = response.data;
      $scope.totalItems = parseInt(response.headers("size"));
    }, function(result) {
      console.log("Can't load news");
      $window.alert("Can't load news");
      $state.go("init.error", {code : result.status, text : "An error while loading news."});
    });
  };

  $scope.page = 1;
  $scope.maxSize = 3;
  $scope.itemsPerPage = NEWS_PER_PAGE;

  $scope.deleteNews = function (newsId) {
    if (confirm("Are you sure?")) {
      NewsFactory.deleteNews(newsId).then(function(){
        $state.reload();
      }, function(result) {
        console.log("Can't delete news");
        $window.alert("Can't delete news");
        $state.go("init.error", {code : result.status, text : "An error while deleting news."});
      });
    }
  };

  function loadPage(callback) {
    $scope.authors = $rootScope.authors;
    $scope.tags = $rootScope.tags;
    callback();
  }

  loadPage(function () {
    $scope.loadNews($scope.page - 1);
  });

}]);
