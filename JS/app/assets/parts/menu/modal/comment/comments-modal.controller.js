angular.module("news-client").controller("CommentsCtrl", ["$scope", "$uibModalInstance",
  function($scope, $uibModalInstance) {

    $scope.tops = [5, 10, 20];

    $scope.selectComments = function (event, top) {
      event.preventDefault();
       $uibModalInstance.close(top);
    };

   $scope.cancel = function () {
     $uibModalInstance.dismiss();
   };

}]);
