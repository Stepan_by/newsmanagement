angular.module("news-client").controller("MenuTagsCtrl", ["$scope", "$uibModalInstance", "tags", "selectedTags", "TagsFactory", "MathFactory", "$window", "$state",
  function($scope, $uibModalInstance, tags, selectedTags, TagsFactory, MathFactory, $window, $state) {

  $scope.tags = MathFactory.getSetDifference(tags, selectedTags);
  $scope.criteriaTags = selectedTags;
  $scope.newTag = "";

  function loadAllTags() {
    TagsFactory.getAllTags().then(function(data) {
      $scope.tags = MathFactory.getSetDifference(data.data, $scope.criteriaTags);
      return data.data;
    }, function (result) {
      console.log("Can't load all tags");
      $window.alert("Can't load all tags");
      $state.go("init.error", {code : result.status, text : "An error while loading all tags."});
    });
  }

  $scope.moveTagToCriteria = function(tag) {
    $scope.criteriaTags.push(tag);
    $scope.tags.splice($scope.tags.indexOf(tag), 1);
  };

  $scope.moveTagFromCriteria = function(tag) {
    $scope.tags.push(tag);
    $scope.criteriaTags.splice($scope.criteriaTags.indexOf(tag), 1);
  };

  $scope.addTag = function (event) {
    event.preventDefault();
    TagsFactory.addTag($scope.newTag).success(function() {
      $scope.newTag = "";
      loadAllTags();
    }, function (result) {
      console.log("Can't save a tag");
      $window.alert("Can't save a tag");
      $state.go("init.error", {code : result.status, text : "An error while saving a tag."});
    });
  };

  $scope.deleteTag = function (tag) {
    TagsFactory.deleteTag(tag).then(function () {
      var notSelectedTags = $scope.tags;
      var selectedTags = $scope.criteriaTags;
      var index = notSelectedTags.indexOf(tag);
      if (index < 0) {
        $scope.criteriaTags.splice(selectedTags.indexOf(tag), 1);
      } else {
        $scope.tags.splice(index, 1);
      }
    });
  };

  $scope.ok = function () {
     $uibModalInstance.close($scope.criteriaTags);
   };

   $scope.closeModal = function() {
     $uibModalInstance.dismiss();
   };

}]);
