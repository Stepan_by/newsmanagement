angular.module("news-client").controller("MenuAuthorsCtrl", ["$scope", "$uibModalInstance", "selectedAuthors", "AuthorsFactory", "MathFactory", "$window", "$state",
  function($scope, $uibModalInstance, selectedAuthors, AuthorsFactory, MathFactory, $window, $state) {

  loadAllAuthors();
  $scope.criteriaAuthors = selectedAuthors;
  $scope.newAuthor = "";

  function loadAllAuthors() {
    AuthorsFactory.getAllAuthors().then(function(data) {
        $scope.authors = MathFactory.getSetDifference(data.data, selectedAuthors);
    }, function (result) {
      console.log("Can't load all authors");
      $window.alert("Can't load all authors");
      $state.go("init.error", {code : result.status, text : "An error while loading all authors."});
    });
  }

  $scope.moveAuthorToCriteria = function(author) {
    $scope.criteriaAuthors.push(author);
    $scope.authors.splice($scope.authors.indexOf(author), 1);
  };

  $scope.moveAuthorFromCriteria = function(author) {
    $scope.authors.push(author);
    $scope.criteriaAuthors.splice($scope.criteriaAuthors.indexOf(author), 1);
  };

  $scope.addAuthor = function (event) {
    event.preventDefault();
    AuthorsFactory.addAuthor($scope.newAuthor).then(function(data) {
      $scope.newAuthor = "";
      loadAllAuthors();
    }, function (result) {
      console.log("Can't save an author");
      $window.alert("Can't save an author");
      $state.go("init.error", {code : result.status, text : "An error while saving author."});
    });
  };

  $scope.removeAuthor = function (author) {
    AuthorsFactory.deleteAuthor(author).then(function (data) {
      var notSelectedAuthors = $scope.authors;
      var selectedAuthors = $scope.criteriaAuthors;
      var index = notSelectedAuthors.indexOf(author);
      if (index < 0) {
        $scope.criteriaAuthors.splice(selectedAuthors.indexOf(author), 1);
      } else {
        $scope.authors.splice(index, 1);
      }
    });
  };

  $scope.ok = function () {
     $uibModalInstance.close($scope.criteriaAuthors);
   };

   $scope.closeModal = function() {
     $uibModalInstance.dismiss();
   };
}]);
