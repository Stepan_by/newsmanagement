angular.module("news-client").controller("MenuCtrl",
["$scope", "$rootScope", "TagsFactory", "$uibModal", "AuthorsFactory", "$state", "$window",
    function($scope, $rootScope, TagsFactory, $uibModal, AuthorsFactory, $state, $window) {

  $scope.criteria = {
    authors : $rootScope.authors,
    tags : $rootScope.tags,
    top : $rootScope.top
  };

  $scope.showCommentsModal = function (event, size) {
    event.preventDefault();
    var commentsModal = $uibModal.open({
      templateUrl: "assets/parts/menu/modal/comment/comments-modal.html",
      controller: "CommentsCtrl",
      size: size
    });
    commentsModal.result.then(function (top) {
      $scope.criteria.top = top;
      $scope.criteria.authors = [];
      $scope.criteria.tags = [];
      $rootScope.top = top;
      $rootScope.authors = [];
      $rootScope.tags = [];
      $state.go("news.commented", {top : top});
    });
  };

  $scope.removeTopComments = function () {
    $scope.criteria.top = null;
    $rootScope.top = null;
    $state.go("default", {}, {reload: true});
  };

  $scope.showTagsModal = function(event, size) {
    event.preventDefault();
    var tagModal = $uibModal.open({
      templateUrl: "assets/parts/menu/modal/tag/menu-tag-modal.html",
      controller: "MenuTagsCtrl",
      size: size,
      resolve: {
        tags: function() {
          return TagsFactory.getAllTags().then(function(data) {
              return data.data;
          }, function (result) {
            console.log("Can't load all tags");
            $window.alert("Can't load all tags");
            $state.go("init.error", {code : result.status, text : "An error while loading all tags."});
          });
        },
        selectedTags : function () {
          return $scope.criteria.tags;
        }
      }
    });
    tagModal.result.then(function (tags) {
      $rootScope.tags = tags;
      $rootScope.top = null;
      $scope.criteria.top = null;
      $scope.criteria.tags = tags;
      $state.go("default", {}, {reload: true});
    });
  };

  $scope.showAuthorsModal = function(event, size) {
    event.preventDefault();
    var authorModal = $uibModal.open({
      templateUrl: "assets/parts/menu/modal/author/menu-author-modal.html",
      controller: "MenuAuthorsCtrl",
      size: size,
      resolve: {
        selectedAuthors : function () {
          return $scope.criteria.authors;
        }
      }
    });
    authorModal.result.then(function (authors) {
      $scope.criteria.top = null;
      $scope.criteria.authors = authors;
      $rootScope.authors = authors;
      $rootScope.top = null;
      $state.go("default", {}, {reload: true});
    });
  };

  $scope.removeAuthor = function(author) {
    $scope.criteria.authors.splice($scope.criteria.authors.indexOf(author), 1);
    $rootScope.authors = $scope.criteria.authors;
    $state.go("default", {}, {reload: true});
  };

  $scope.removeTag = function(tag) {
    $scope.criteria.tags.splice($scope.criteria.tags.indexOf(tag), 1);
    $rootScope.tags = $scope.criteria.tags;
    $state.go("default", {}, { reload: true});
  };

  $scope.refreshCriteria = function () {
    $scope.criteria.top = null;
    $scope.criteria.authors = [];
    $scope.criteria.tags = [];
    $rootScope.tags = [];
    $rootScope.authors = [];
    $rootScope.top = null;
    $state.go("default", {}, {reload: true});
  };

}]);
