angular.module("news-client").config(["$stateProvider", function ($stateProvider) {

  $stateProvider.state("init.error", {
    url: "/error",
    views : {
      "content@": {
        templateUrl: "assets/parts/error/error.html",
        controller: "ErrorCtrl"
      }
    },
    params: {
      code: null,
      text: ""
    }
  });
}]);
