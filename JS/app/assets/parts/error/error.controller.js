angular.module("news-client").controller("ErrorCtrl", ["$scope", "$stateParams", function($scope, $stateParams) {

  $scope.code = $stateParams.code;
  $scope.text = $stateParams.text;
}]);
