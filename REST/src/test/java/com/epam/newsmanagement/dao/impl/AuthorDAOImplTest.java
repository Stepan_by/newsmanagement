package com.epam.newsmanagement.dao.impl;

import com.epam.newsmanagement.config.ContextConfig;
import com.epam.newsmanagement.dao.AuthorDAO;
import com.epam.newsmanagement.dao.exception.DAOException;
import com.epam.newsmanagement.domain.Author;
import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseOperation;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import com.github.springtestdbunit.annotation.DatabaseTearDown;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.transaction.annotation.Transactional;

import static org.junit.Assert.*;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = ContextConfig.class)
@DatabaseSetup(value = "/data/author-test.xml", type = DatabaseOperation.REFRESH)
@TestExecutionListeners({
        DependencyInjectionTestExecutionListener.class,
        DbUnitTestExecutionListener.class,
        TransactionalTestExecutionListener.class})
@DatabaseTearDown(value = "/data/author-test.xml", type = DatabaseOperation.DELETE)
@WebAppConfiguration
@Transactional
public class AuthorDAOImplTest {

    @Autowired
    private AuthorDAO authorDAO;

    private Author author;

    @Before
    public void setUp() throws Exception {
        author = new Author("Name");
    }

    @Test
    public void findById() throws Exception {
        Author author = authorDAO.find(1L);
        assertEquals(1, author.getId());
    }

    @Test
    public void add() throws Exception {
        Author addedAuthor = authorDAO.add(author);
        Author author = authorDAO.find(addedAuthor.getId());
        assertEquals(addedAuthor, author);
        authorDAO.delete(addedAuthor.getId());
    }

    @Test
    public void find() throws Exception {
        Author author = new Author(3L, "Rick");
        assertEquals(author, authorDAO.find("Rick"));
    }

    @Test
    public void update() throws Exception {
        Author author = authorDAO.find(2L);
        author.setName("Dave");
        authorDAO.update(author);
        assertEquals(author, authorDAO.find("Dave"));
    }

    @Test(expected = DAOException.class)
    public void delete() throws Exception {
        long id = 1;
        assertNotNull(authorDAO.find(id));
        authorDAO.delete(id);
        assertNull(authorDAO.find(id));
    }
}
