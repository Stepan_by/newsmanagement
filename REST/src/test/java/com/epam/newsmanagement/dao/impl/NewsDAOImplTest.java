package com.epam.newsmanagement.dao.impl;

import com.epam.newsmanagement.config.ContextConfig;
import com.epam.newsmanagement.dao.NewsDAO;
import com.epam.newsmanagement.dao.exception.DAOException;
import com.epam.newsmanagement.domain.Comment;
import com.epam.newsmanagement.domain.News;
import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseOperation;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import com.github.springtestdbunit.annotation.DatabaseTearDown;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.transaction.annotation.Transactional;

import java.sql.Date;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static org.junit.Assert.*;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = ContextConfig.class)
@DatabaseSetup(value = "/data/news-test.xml", type = DatabaseOperation.REFRESH)
@TestExecutionListeners({
        DependencyInjectionTestExecutionListener.class,
        DbUnitTestExecutionListener.class,
        TransactionalTestExecutionListener.class})
@DatabaseTearDown(value = "/data/news-test.xml", type = DatabaseOperation.DELETE)
@WebAppConfiguration
@Transactional
public class NewsDAOImplTest {

    @Autowired
    private NewsDAO newsDAO;

    @Test
    public void add() throws Exception {
        News news = new News("Title", "Short", "Full", new Date(2131242141));
        Set<Comment> comments = new HashSet<>();
        comments.add(new Comment(news, "First text", new Date(new SimpleDateFormat("yyyy-mm-dd").parse("2012-12-21").getTime())));
        comments.add(new Comment(news, "Second text", new Date(new SimpleDateFormat("yyyy-mm-dd").parse("2012-12-02").getTime())));
        news.setComments(comments);
        news = newsDAO.add(news);
        assertEquals(news, newsDAO.find(news.getId()));
    }

    @Test
    public void find() throws Exception {
        assertEquals(new Long(1L), newsDAO.find(1L).getId());
    }

    @Test
    public void update() throws Exception {
        String title = "new title";
        long id = 1;
        News news = newsDAO.find(id);
        news.setTitle(title);
        newsDAO.update(news);
        assertEquals(title, newsDAO.find(id).getTitle());
    }

    @Test(expected = DAOException.class)
    public void delete() throws Exception {
        long id = 3;
        assertNotNull(newsDAO.find(id));
        newsDAO.delete(id);
        assertNull(newsDAO.find(id));
    }
}