package com.epam.newsmanagement.config;

import com.github.springtestdbunit.bean.DatabaseDataSourceConnectionFactoryBean;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.*;
import org.springframework.core.env.Environment;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import javax.annotation.Resource;
import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;

@Configuration
@EnableTransactionManagement
@ComponentScan("com.epam.newsmanagement")
@PropertySource("classpath:db.properties")
@Import(DatabaseContext.class)
public class ContextConfig {

    @Autowired
    private EntityManagerFactory entityManagerFactory;

    @Autowired
    private DataSource dataSource;

    @Resource
    private Environment environment;

    @Bean
    public PlatformTransactionManager transactionManager(EntityManagerFactory entityManagerFactory) {
        return new JpaTransactionManager(entityManagerFactory);
    }

    @Bean(name = "dbUnitDatabaseConnection")
    public DatabaseDataSourceConnectionFactoryBean dbUnitDataSource() {
        DatabaseDataSourceConnectionFactoryBean source = new DatabaseDataSourceConnectionFactoryBean(dataSource);
        source.setSchema(environment.getRequiredProperty("db_username"));
        return source;
    }

}
