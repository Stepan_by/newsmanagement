package com.epam.newsmanagement.service.impl;

import com.epam.newsmanagement.config.ContextConfig;
import com.epam.newsmanagement.dao.NewsDAO;
import com.epam.newsmanagement.domain.Author;
import com.epam.newsmanagement.domain.Comment;
import com.epam.newsmanagement.domain.News;
import com.epam.newsmanagement.domain.Tag;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import java.sql.Date;
import java.text.SimpleDateFormat;
import java.util.*;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.Mockito.*;
import static org.mockito.MockitoAnnotations.initMocks;

@ContextConfiguration(classes = ContextConfig.class)
@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration
public class NewsServiceImplTest {

    @InjectMocks
    private NewsServiceImpl newsService;

    @Mock
    private NewsDAO newsDAO;

    private News news;

    @Before
    public void setUp() throws Exception {
        initMocks(this);
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-mm-dd");
        news = new News("Title", "Short", "Full", new Date(2131242141));
        Date creationDate = new Date(dateFormat.parse("2012-06-05").getTime());
        Set<Comment> comments = new HashSet<>();
        comments.add(new Comment(news, "First text", creationDate));
        comments.add(new Comment(news, "Second text", creationDate));
        Set<Author> authors = new HashSet<>();
        authors.add(new Author("Mike"));
        authors.add(new Author("Kevin"));
        Set<Tag> tags = new HashSet<>();
        tags.add(new Tag("B"));
        tags.add(new Tag("A"));
        news.setComments(comments);
        news.setTags(tags);
        news.setAuthors(authors);
        when(newsDAO.add(any(News.class))).thenReturn(news);
        when(newsDAO.update(any(News.class))).thenReturn(news);
        when(newsDAO.find(anyLong())).thenReturn(news);
        when(newsDAO.delete(anyLong())).thenReturn(news);
    }

    @Test
    public void add() throws Exception {
        News anyNews = new News();
        News addedNews = newsDAO.add(anyNews);
        assertEquals(news, addedNews);
    }

    @Test
    public void find() throws Exception {
        News foundNews = newsDAO.find(3L);
        assertNotNull(foundNews);
        assertEquals(news, foundNews);
    }

    @Test
    public void delete() throws Exception {
        assertEquals(news, newsDAO.delete(1L));
    }

}