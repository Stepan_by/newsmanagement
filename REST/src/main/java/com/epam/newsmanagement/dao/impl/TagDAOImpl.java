package com.epam.newsmanagement.dao.impl;

import com.epam.newsmanagement.dao.TagDAO;
import com.epam.newsmanagement.domain.News;
import com.epam.newsmanagement.domain.Tag;
import com.epam.newsmanagement.dao.exception.DAOException;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.List;

/**
 * Tag DAO implementation
 */
@Repository
public class TagDAOImpl implements TagDAO {

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public Tag add(Tag tag) throws DAOException {
        entityManager.persist(tag);
        return tag;
    }

    @Override
    public List<Tag> findAllTags() throws DAOException {
        CriteriaBuilder cb = entityManager.getCriteriaBuilder();
        CriteriaQuery<Tag> criteriaQuery = cb.createQuery(Tag.class);
        Root<Tag> root = criteriaQuery.from(Tag.class);
        return entityManager.createQuery(criteriaQuery.select(root)).getResultList();
    }

    @Override
    public Tag find(String name) throws DAOException {
        Tag tag = entityManager.find(Tag.class, name);
        if (tag == null) {
            throw new DAOException("There is tag with name = " + name);
        }
        return tag;
    }

    @Override
    public Tag update(Tag tag) throws DAOException {
        return entityManager.merge(tag);
    }

    @Override
    public Tag delete(String tagName) throws DAOException {
        Tag tag = find(tagName);
        entityManager.remove(tag);
        return tag;
    }

    @Override
    public void deleteTagRelationships(String tagName) {
        Tag tag = entityManager.find(Tag.class, tagName);
        for (News news : tag.getNews()) {
            news.getTags().remove(tag);
            entityManager.merge(news);
        }
    }
}
