package com.epam.newsmanagement.dao;

import com.epam.newsmanagement.domain.Author;
import com.epam.newsmanagement.dao.exception.DAOException;

import java.util.List;

/**
 * Author DAO interface
 */
public interface AuthorDAO extends GenericDAO<Author, Long> {

    /**
     * Adds author to the database
     * @param author author that is adding
     * @return author's id
     */
    @Override
    Author add(Author author) throws DAOException;

    /**
     * Fins author by his id
     * @param authorId author's id
     * @return {@link Author} with required id or null if it wasn't find
     */
    @Override
    Author find(Long authorId) throws DAOException;

    /**
     * Returns all authors in the application
     * @return list of authors
     */
    List findAllAuthors() throws DAOException;

    /**
     * Finds author by his name
     * @param name author's name
     * @return {@link Author} with required name or null if it wasn't find
     */
    Author find(String name) throws DAOException;

    /**
     * Updates author
     * @param author updated author
     */
    @Override
    Author update(Author author) throws DAOException;

    /**
     * Deletes author from the database by his id
     * @param authorId author's id
     */
    @Override
    Author delete(Long authorId) throws DAOException;

    /**
     * Delete all author relationships
     * @param authorId author's id
     */
    void deleteAuthorRelationships(Long authorId);
}
