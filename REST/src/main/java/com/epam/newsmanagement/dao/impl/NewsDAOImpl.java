package com.epam.newsmanagement.dao.impl;

import com.epam.newsmanagement.dao.NewsDAO;
import com.epam.newsmanagement.domain.Author;
import com.epam.newsmanagement.domain.Comment;
import com.epam.newsmanagement.domain.News;
import com.epam.newsmanagement.domain.Tag;
import com.epam.newsmanagement.dto.SearchCriteria;
import com.epam.newsmanagement.dao.exception.DAOException;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.*;
import java.util.ArrayList;
import java.util.List;

/**
 * News DAO implementation
 */
@Repository
public class NewsDAOImpl implements NewsDAO {

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public News add(News news) throws DAOException {
        entityManager.persist(news);
        return news;
    }

    @Override
    public News find(Long newsId) throws DAOException {
        News news = entityManager.find(News.class, newsId);
        if (news == null) {
            throw new DAOException("There is no news with id = " + newsId);
        }
        return news;
    }

    @Override
    public News update(News news) throws DAOException {
        return entityManager.merge(news);
    }

    @Override
    public News delete(Long newsId) throws DAOException {
        News news = find(newsId);
        entityManager.remove(news);
        return news;
    }

    @Override
    public List<News> findNewsAtPage(SearchCriteria searchCriteria) throws DAOException {
        CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery<News> criteriaQuery = criteriaBuilder.createQuery(News.class);
        Root<News> root = criteriaQuery.from(News.class);
        List<Predicate> predicates = new ArrayList<>();
         if (!searchCriteria.getAuthorsIds().isEmpty()) {
            Subquery<News> authorSubquery = criteriaQuery.subquery(News.class);
            Root<News> newsAuthorRoot = authorSubquery.from(News.class);
            Join<News, Author> newsAuthorJoin = newsAuthorRoot.join("authors");
            authorSubquery.select(newsAuthorRoot.get("id")).
                    where(newsAuthorJoin.get("id").in(searchCriteria.getAuthorsIds())).
                    groupBy(newsAuthorRoot.get("id")).
                    having(criteriaBuilder.equal(criteriaBuilder.count(newsAuthorJoin.get("id")), searchCriteria.getAuthorsIds().size()));
            predicates.add(root.get("id").in(authorSubquery));
        }
        if (!searchCriteria.getTagsNames().isEmpty()) {
            Subquery<News> tagSubquery = criteriaQuery.subquery(News.class);
            Root<News> newsTagRoot = tagSubquery.from(News.class);
            Join<News, Tag> newsTagJoin = newsTagRoot.join("tags");
            tagSubquery.select(newsTagRoot.get("id")).
                    where(newsTagJoin.get("name").in(searchCriteria.getTagsNames())).
                    groupBy(newsTagRoot.get("id")).
                    having(criteriaBuilder.equal(criteriaBuilder.count(newsTagJoin.get("name")), searchCriteria.getTagsNames().size()));
            predicates.add(root.get("id").in(tagSubquery));
        }
        criteriaQuery.
                distinct(true).
                select(root).
                where(predicates.toArray(new Predicate[predicates.size()])).
                orderBy(criteriaBuilder.desc(root.get("creationDate")));
        return entityManager.
                createQuery(criteriaQuery).
                setFirstResult(searchCriteria.getFrom().intValue()).
                setMaxResults(searchCriteria.getNumber().intValue()).
                getResultList();
    }

    @Override
    public List<News> findTopNews(Long from, Long number) throws DAOException {
        CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery<News> criteriaQuery = criteriaBuilder.createQuery(News.class);
        Root<News> root = criteriaQuery.from(News.class);
        Join<News, Comment> join = root.join("comments", JoinType.LEFT);
        criteriaQuery.select(root).groupBy(root.get("id"),
                root.get("title"),
                root.get("shortText"),
                root.get("fullText"),
                root.get("creationDate"),
                root.get("modificationDate")).orderBy(criteriaBuilder.desc(criteriaBuilder.count(join.get("id"))));
        return entityManager.
                createQuery(criteriaQuery).
                setFirstResult(from.intValue()).
                setMaxResults(number.intValue()).
                getResultList();
    }

    @Override
    public Long countNews(SearchCriteria searchCriteria) throws DAOException {
        CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery<Long> criteriaQuery = criteriaBuilder.createQuery(Long.class);
        Root<News> root = criteriaQuery.from(News.class);
        List<Predicate> predicates = new ArrayList<>();
        if (!searchCriteria.getAuthorsIds().isEmpty()) {
            Subquery<News> authorSubquery = criteriaQuery.subquery(News.class);
            Root<News> newsAuthorRoot = authorSubquery.from(News.class);
            Join<News, Author> newsAuthorJoin = newsAuthorRoot.join("authors");
            authorSubquery.select(newsAuthorRoot.get("id")).
                    where(newsAuthorJoin.get("id").in(searchCriteria.getAuthorsIds())).
                    groupBy(newsAuthorRoot.get("id")).
                    having(criteriaBuilder.equal(criteriaBuilder.count(newsAuthorJoin.get("id")), searchCriteria.getAuthorsIds().size()));
            predicates.add(root.get("id").in(authorSubquery));
        }
        if (!searchCriteria.getTagsNames().isEmpty()) {
            Subquery<News> tagSubquery = criteriaQuery.subquery(News.class);
            Root<News> newsTagRoot = tagSubquery.from(News.class);
            Join<News, Tag> newsTagJoin = newsTagRoot.join("tags");
            tagSubquery.select(newsTagRoot.get("id")).
                    where(newsTagJoin.get("name").in(searchCriteria.getTagsNames())).
                    groupBy(newsTagRoot.get("id")).
                    having(criteriaBuilder.equal(criteriaBuilder.count(newsTagJoin.get("name")), searchCriteria.getTagsNames().size()));
            predicates.add(root.get("id").in(tagSubquery));
        }
        criteriaQuery.select(criteriaBuilder.count(root.get("id"))).
                where(predicates.toArray(new Predicate[predicates.size()]));
        return entityManager.createQuery(criteriaQuery).getSingleResult();
    }

    @Override
    public List<Comment> findComments(Long newsId, Long from, Long number) {
        CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery<Comment> criteriaQuery = criteriaBuilder.createQuery(Comment.class);
        Root<Comment> root = criteriaQuery.from(Comment.class);
        criteriaQuery.
                where(criteriaBuilder.equal(root.get("news").get("id"), newsId)).
                orderBy(criteriaBuilder.desc(root.get("creationDate")));
        return entityManager.
                createQuery(criteriaQuery).
                setFirstResult(from.intValue()).
                setMaxResults(number.intValue()).
                getResultList();
    }
}
