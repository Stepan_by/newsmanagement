package com.epam.newsmanagement.dao;

import com.epam.newsmanagement.domain.Tag;
import com.epam.newsmanagement.dao.exception.DAOException;

import java.util.List;

/**
 * DAO Tag Interface for working with a database
 */
public interface TagDAO extends GenericDAO<Tag, String> {

    /**
     * Add tag to the database
     * @param tag tag that is adding
     * @return true if the tag was added
     */
    @Override
    Tag add(Tag tag) throws DAOException;

    /**
     * Find a tag in the database
     * @param tagName tag's name
     * @return tag that was found
     * @throws DAOException
     */
    @Override
    Tag find(String tagName) throws DAOException;

    /**
     * Returns all tags
     * @return list of tags
     */
    List<Tag> findAllTags() throws DAOException;

    /**
     * Update the tag
     * @param tag tag that is updating
     * @throws DAOException
     */
    @Override
    Tag update(Tag tag) throws DAOException;

    /**
     * Delete the tag from the database
     * @param tagName tag's tagId
     */
    @Override
    Tag delete(String tagName) throws DAOException;

    void deleteTagRelationships(String tagName);
}
