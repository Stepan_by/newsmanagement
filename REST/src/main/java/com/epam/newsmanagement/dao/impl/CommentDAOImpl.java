package com.epam.newsmanagement.dao.impl;

import com.epam.newsmanagement.dao.CommentDAO;
import com.epam.newsmanagement.domain.Comment;
import com.epam.newsmanagement.domain.News;
import com.epam.newsmanagement.dao.exception.DAOException;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

/**
 * Comments DAO implementation
 */
@Repository
public class CommentDAOImpl implements CommentDAO {

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public Comment add(Comment comment) throws DAOException {
        entityManager.persist(comment);
        return comment;
    }

    @Override
    public Comment find(Long commentId) throws DAOException {
        Comment comment = entityManager.find(Comment.class, commentId);
        if (comment == null) {
            throw new DAOException("There is no comment with id = " + commentId);
        }
        return comment;
    }

    @Override
    public Comment update(Comment comment) throws DAOException {
        entityManager.merge(comment);
        return comment;
    }

    @Override
    public Comment delete(Long commentId) throws DAOException {
        Comment comment = find(commentId);
        entityManager.remove(comment);
        return comment;
    }

    @Override
    public Long countComments(Long newsId) throws DAOException {
        CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery<Long> criteriaQuery = criteriaBuilder.createQuery(Long.class);
        Root<News> root = criteriaQuery.from(News.class);
        criteriaQuery.
                select(criteriaBuilder.count(root.join("comments").get("id"))).
                where(criteriaBuilder.equal(root.get("id"), newsId));
        return entityManager.
                createQuery(criteriaQuery).
                getSingleResult();
    }
}
