package com.epam.newsmanagement.dao;

import com.epam.newsmanagement.domain.Comment;
import com.epam.newsmanagement.dao.exception.DAOException;

/**
 * DAO Comments Interface for working with a database
 */
public interface CommentDAO extends GenericDAO<Comment, Long> {

    /**
     * Add comment to a news
     * @param comment the comment that is adding
     * @return true if the comment was added
     */
    @Override
    Comment add(Comment comment) throws DAOException;

    /**
     * Find a comment in the database
     * @param commentId comment's commentId
     * @return comment that was found
     * @throws DAOException
     */
    @Override
    Comment find(Long commentId) throws DAOException;

    /**
     * Update comment
     * @param comment comment that is updating
     * @throws DAOException
     */
    @Override
    Comment update(Comment comment) throws DAOException;

    /**
     * Delete the comment from the database
     * @param commentId comment's commentId
     */
    @Override
    Comment delete(Long commentId) throws DAOException;

    Long countComments(Long newsId) throws DAOException;
}
