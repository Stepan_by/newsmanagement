package com.epam.newsmanagement.dao.impl;

import com.epam.newsmanagement.dao.AuthorDAO;
import com.epam.newsmanagement.domain.Author;
import com.epam.newsmanagement.domain.News;
import com.epam.newsmanagement.dao.exception.DAOException;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.List;

@Repository
public class AuthorDAOImpl implements AuthorDAO {

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public Author add(Author author) throws DAOException {
        entityManager.persist(author);
        return author;
    }

    @Override
    public Author find(Long authorId) throws DAOException {
        Author author = entityManager.find(Author.class, authorId);
        if (author == null) {
            throw new DAOException("There is no author with id = " + authorId);
        }
        return author;
    }

    @Override
    public List<Author> findAllAuthors() throws DAOException {
        CriteriaQuery<Author> criteriaQuery = entityManager.getCriteriaBuilder().createQuery(Author.class);
        Root<Author> root = criteriaQuery.from(Author.class);
        return entityManager.createQuery(criteriaQuery.select(root)).getResultList();
    }

    @Override
    public Author find(String name) throws DAOException {
        CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery<Author> criteriaQuery = criteriaBuilder.createQuery(Author.class);
        Root<Author> root = criteriaQuery.from(Author.class);
        criteriaQuery.select(root).where(criteriaBuilder.equal(root.get("name"), name));
        return entityManager.createQuery(criteriaQuery).getSingleResult();
    }

    @Override
    public Author update(Author author) throws DAOException {
        return entityManager.merge(author);
    }

    @Override
    public Author delete(Long authorId) throws DAOException {
        Author author = find(authorId);
        entityManager.remove(author);
        return author;
    }

    @Override
    public void deleteAuthorRelationships(Long authorId) {
        Author author = entityManager.find(Author.class, authorId);
        for (News news : author.getNews()) {
            news.getAuthors().remove(author);
            entityManager.merge(news);
        }
    }
}
