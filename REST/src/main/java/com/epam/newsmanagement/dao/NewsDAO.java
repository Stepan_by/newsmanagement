package com.epam.newsmanagement.dao;

import com.epam.newsmanagement.domain.Comment;
import com.epam.newsmanagement.domain.News;
import com.epam.newsmanagement.dto.SearchCriteria;
import com.epam.newsmanagement.dao.exception.DAOException;

import java.util.List;

/**
 * DAO News Interface for working with a database
 */
public interface NewsDAO extends GenericDAO<News, Long> {

    /**
     * Add the news to a database
     * @param news news that is adding
     * @return true if the news was added
     */
    @Override
    News add(News news) throws DAOException;

    /**
     * Find a news by its newsId
     * @param newsId news's newsId
     * @return the news that was found
     */
    @Override
    News find(Long newsId) throws DAOException;

    /**
     * Updating the news information
     * @param news news that is correcting
     */
    @Override
    News update(News news) throws DAOException;

    /**
     * Delete the news from a database
     * @param newsId news's newsId
     */
    @Override
    News delete(Long newsId) throws DAOException;

    /**
     * Get all news from a database
     * @return list of news
     */
    List<News> findNewsAtPage(SearchCriteria searchCriteria) throws DAOException;

    List<News> findTopNews(Long from, Long number) throws DAOException;

    /**
     * Count all news in the database
     * @return number of the news
     */
    Long countNews(SearchCriteria searchCriteria) throws DAOException;

    List<Comment> findComments(Long newsId, Long from, Long number) throws DAOException;
}
