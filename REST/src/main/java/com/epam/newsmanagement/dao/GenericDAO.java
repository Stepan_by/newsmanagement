package com.epam.newsmanagement.dao;

import com.epam.newsmanagement.dao.exception.DAOException;

/**
 * Generic interface with basic CRUD operations
 * @param <Entity> the first parameter
 * @param <Key> the second parameter
 */
public interface GenericDAO<Entity, Key> {

    /**
     * Create operation
     * @param entity the domain that is adding
     * @return id of the added object
     * @throws DAOException in case of any exception this exception will be thrown
     */
    Entity add(Entity entity) throws DAOException;

    /**
     * Find an domain by its key
     * @param key the domain's key
     * @return an domain
     */
    Entity find(Key key) throws DAOException;

    /**
     * Update an domain
     * @param entity domain that is updating
     */
    Entity update(Entity entity) throws DAOException;

    /**
     * Delete an domain by its key
     * @param key the domain's key
     */
    Entity delete(Key key) throws DAOException;
}
