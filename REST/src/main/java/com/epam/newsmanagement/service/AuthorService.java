package com.epam.newsmanagement.service;

import com.epam.newsmanagement.domain.Author;
import com.epam.newsmanagement.service.exception.ServiceException;

import java.util.List;

/**
 * Author service implementation
 */
public interface AuthorService extends GenericService<Author, Long> {

    /**
     * Adds author to the system
     * @param author author that is adding
     * @return author's id
     */
    @Override
    Author add(Author author) throws ServiceException;

    /**
     * Gets author from the system ny its id
     * @param authorId author's id
     * @return author
     */
    @Override
    Author find(Long authorId) throws ServiceException;

    /**
     * Returns all authors in the application
     * @return list of authors
     */
    List<Author> findAllAuthors() throws ServiceException;

    /**
     * Updates author
     * @param author updated author
     */
    @Override
    Author update(Author author) throws ServiceException;

    /**
     * Deletes author by his id
     * @param authorId author's id
     */
    @Override
    Author delete(Long authorId) throws ServiceException;
}
