package com.epam.newsmanagement.service.impl;

import com.epam.newsmanagement.dao.TagDAO;
import com.epam.newsmanagement.domain.Tag;
import com.epam.newsmanagement.dao.exception.DAOException;
import com.epam.newsmanagement.service.exception.ServiceException;
import com.epam.newsmanagement.service.TagService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class TagServiceImpl implements TagService {

    @Autowired
    private TagDAO tagDAO;

    @Override
    public Tag add(Tag tag) throws ServiceException {
        try {
            return tagDAO.add(tag);
        } catch (DAOException e) {
            throw new ServiceException(e);
        }
    }

    @Override
    public List<Tag> findAllTags() throws ServiceException {
        try {
            return tagDAO.findAllTags();
        } catch (DAOException e) {
            throw new ServiceException(e);
        }
    }

    @Override
    public Tag find(String tagName) throws ServiceException {
        try {
            return tagDAO.find(tagName);
        } catch (DAOException e) {
            throw new ServiceException(e);
        }
    }

    @Override
    public Tag update(Tag tag) throws ServiceException {
        try {
            return tagDAO.update(tag);
        } catch (DAOException e) {
            throw new ServiceException(e);
        }
    }

    @Override
    public Tag delete(String tagName) throws ServiceException {
        try {
            tagDAO.deleteTagRelationships(tagName);
            return tagDAO.delete(tagName);
        } catch (DAOException e) {
            throw new ServiceException(e);
        }
    }
}
