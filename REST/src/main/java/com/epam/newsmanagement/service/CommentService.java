package com.epam.newsmanagement.service;

import com.epam.newsmanagement.domain.Comment;
import com.epam.newsmanagement.service.exception.ServiceException;

public interface CommentService extends GenericService<Comment, Long> {

    /**
     * Adds comment to a news
     * @param comment the comment that is adding
     * @return true if the comment was added
     */
    @Override
    Comment add(Comment comment) throws ServiceException;

    /**
     * Add comment if has only news id
     * @param newsId news id
     * @param comment comment for news
     * @return added comment
     */
    Comment add(Long newsId, Comment comment) throws ServiceException;

    /**
     * Finds a comment in the system
     * @param commentId comment's commentId
     * @return comment that was found
     */
    @Override
    Comment find(Long commentId) throws ServiceException;

    /**
     * Updates comment
     * @param comment comment that is updating
     */
    Comment update(Comment comment) throws ServiceException;

    /**
     * Deletes comment
     * @param commentId comment's commentId
     */
    Comment delete(Long commentId) throws ServiceException;

    Long countComments(Long newsId) throws ServiceException;
}
