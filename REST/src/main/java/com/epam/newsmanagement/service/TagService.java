package com.epam.newsmanagement.service;

import com.epam.newsmanagement.domain.Tag;
import com.epam.newsmanagement.service.exception.ServiceException;

import java.util.List;

public interface TagService extends GenericService<Tag, String> {

    /**
     * Adds tag to the system
     * @param tag tag that is adding
     * @return true if the tag was added
     */
    @Override
    Tag add(Tag tag) throws ServiceException;

    /**
     * Returns all tags
     * @return list of tags
     */
    List<Tag> findAllTags() throws ServiceException;

    /**
     * Finds a tag in the system
     * @param tagName tag's tagId
     * @return tag that was found
     */
    @Override
    Tag find(String tagName) throws ServiceException;

    /**
     * Updates tag
     * @param tag tag that is updating
     */
    @Override
    Tag update(Tag tag) throws ServiceException;

    /**
     * Deletes tag
     * @param tagName tag's tagId
     */
    @Override
    Tag delete(String tagName) throws ServiceException;
}
