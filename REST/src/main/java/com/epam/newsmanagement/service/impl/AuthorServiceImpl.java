package com.epam.newsmanagement.service.impl;

import com.epam.newsmanagement.dao.AuthorDAO;
import com.epam.newsmanagement.domain.Author;
import com.epam.newsmanagement.dao.exception.DAOException;
import com.epam.newsmanagement.service.exception.ServiceException;
import com.epam.newsmanagement.service.AuthorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class AuthorServiceImpl implements AuthorService {

    @Autowired
    private AuthorDAO authorDAO;

    @Override
    public Author add(Author author) throws ServiceException {
        try {
            return authorDAO.add(author);
        } catch (DAOException e) {
            throw new ServiceException(e);
        }
    }

    @Override
    public Author find(Long authorId) throws ServiceException {
        try {
            return authorDAO.find(authorId);
        } catch (DAOException e) {
            throw new ServiceException(e);
        }
    }

    @Override
    public List<Author> findAllAuthors() throws ServiceException {
        try {
            return authorDAO.findAllAuthors();
        } catch (DAOException e) {
            throw new ServiceException(e);
        }
    }

    @Override
    public Author update(Author author) throws ServiceException {
        try {
            return authorDAO.update(author);
        } catch (DAOException e) {
            throw new ServiceException(e);
        }
    }

    @Override
    public Author delete(Long authorId) throws ServiceException {
        try {
            authorDAO.deleteAuthorRelationships(authorId);
            return authorDAO.delete(authorId);
        } catch (DAOException e) {
            throw new ServiceException(e);
        }
    }
}
