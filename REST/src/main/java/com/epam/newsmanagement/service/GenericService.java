package com.epam.newsmanagement.service;

import com.epam.newsmanagement.service.exception.ServiceException;

/**
 * Generic interface for all services that provide CRUD operations
 * @param <Entity> Entity for the future implementations
 * @param <Key> key for the future implementations
 */
public interface GenericService<Entity, Key> {

    /**
     * Adds domain to the system
     * @param entity adding domain
     * @return key of the added domain
     */
    Entity add(Entity entity) throws ServiceException;

    /**
     * Gets an domain by its key
     * @param key the domain's key
     * @return required domain
     */
    Entity find(Key key) throws ServiceException;

    /**
     * Updates domain
     * @param entity updated domain
     */
    Entity update(Entity entity) throws ServiceException;

    /**
     * Deletes domain by its key
     * @param key domain's key
     */
    Entity delete(Key key) throws ServiceException;
}
