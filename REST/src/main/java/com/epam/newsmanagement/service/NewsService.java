package com.epam.newsmanagement.service;

import com.epam.newsmanagement.domain.Comment;
import com.epam.newsmanagement.domain.News;
import com.epam.newsmanagement.dto.SearchCriteria;
import com.epam.newsmanagement.service.exception.ServiceException;
import com.epam.newsmanagement.wrapper.Page;

import java.util.List;

/**
 * News service implementation
 */
public interface NewsService extends GenericService<News, Long> {

    /**
     * Adds user to the system
     * @param news news that is adding
     * @return news's id
     */
    @Override
    News add(News news) throws ServiceException;

    /**
     * Gets news by its id from the system
     * @param newsId news's id
     * @return news with {@param newsId}
     */
    @Override
    News find(Long newsId) throws ServiceException;

    /**
     * Updates news
     * @param news updated news
     */
    @Override
    News update(News news) throws ServiceException;

    /**
     * Deletes news
     * @param newsId news's id
     */
    @Override
    News delete(Long newsId) throws ServiceException;

    /**
     * All news in range
     * @param criteria criteria
     * @return list of news
     */
    Page<News> findNewsAtPage (SearchCriteria criteria) throws ServiceException;

    /**
     * Returns top news
     * @param from news from
     * @param number number of news
     * @param topNumber top number
     * @return list of top news
     */
    List<News> findTopNews(Long from, Long number, Long topNumber) throws ServiceException;

    /**
     * Returns the number of news by given criteria
     * @param searchCriteria search criteria
     * @return list of news
     */
    Long countNews(SearchCriteria searchCriteria) throws ServiceException;

    Page<Comment> findCommentsAtPage(Long newsId, Long from, Long number) throws ServiceException;
}
