package com.epam.newsmanagement.service.impl;

import com.epam.newsmanagement.dao.NewsDAO;
import com.epam.newsmanagement.domain.Comment;
import com.epam.newsmanagement.domain.News;
import com.epam.newsmanagement.dto.SearchCriteria;
import com.epam.newsmanagement.dao.exception.DAOException;
import com.epam.newsmanagement.service.exception.ServiceException;
import com.epam.newsmanagement.service.CommentService;
import com.epam.newsmanagement.service.NewsService;
import com.epam.newsmanagement.wrapper.Page;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.Calendar;
import java.util.List;

@Service
@Transactional
public class NewsServiceImpl implements NewsService {

    @Autowired
    private NewsDAO newsDAO;
    @Autowired
    private CommentService commentService;

    private static final Long FROM = 0L;
    private static final Long NUMBER = 5L;
    private static final Long TOP_NUMBER = 10L;

    @Override
    public News add(News news) throws ServiceException {
        Date currentDate = new Date(Calendar.getInstance().getTimeInMillis());
        try {
            news.setCreationDate(currentDate);
            news.setModificationDate(currentDate);
            return newsDAO.add(news);
        } catch (DAOException e) {
            throw new ServiceException(e);
        }
    }

    @Override
    public News find(Long newsId) throws ServiceException {
        try {
            return newsDAO.find(newsId);
        } catch (DAOException e) {
            throw new ServiceException(e);
        }
    }

    @Override
    public News update(News news) throws ServiceException {
        news.setModificationDate(new Date(Calendar.getInstance().getTimeInMillis()));
        try {
            return newsDAO.update(news);
        } catch (DAOException e) {
            throw new ServiceException(e);
        }
    }

    @Override
    public News delete(Long newsId) throws ServiceException {
        try {
            return newsDAO.delete(newsId);
        } catch (DAOException e) {
            throw new ServiceException(e);
        }
    }

    @Override
    public Page<News> findNewsAtPage(SearchCriteria criteria) throws ServiceException {
        try {
            if (criteria.getFrom() == null) {
                criteria.setFrom(FROM);
            }
            if (criteria.getNumber() == null) {
                criteria.setNumber(NUMBER);
            }
            return new Page<>(newsDAO.findNewsAtPage(criteria),
                    criteria.getFrom(),
                    criteria.getNumber(),
                    newsDAO.countNews(criteria));
        } catch (DAOException e) {
            throw new ServiceException(e);
        }
    }

    @Override
    public List<News> findTopNews(Long from, Long number, Long topNumber) throws ServiceException {
        from = from == null ? FROM : from;
        number = number == null ? topNumber : number;
        topNumber = topNumber == null ? TOP_NUMBER : topNumber;
        try {
            if (from + number > topNumber) {
                number = topNumber - from;
            }
            return newsDAO.findTopNews(from, number);
        } catch (DAOException e) {
            throw new ServiceException(e);
        }
    }

    @Override
    public Long countNews(SearchCriteria searchCriteria) throws ServiceException {
        try {
            return newsDAO.countNews(searchCriteria);
        } catch (DAOException e) {
            throw new ServiceException(e);
        }
    }

    @Override
    public Page<Comment> findCommentsAtPage(Long newsId, Long from, Long number) throws ServiceException {
        from = from == null ? FROM : from;
        number = number == null ? NUMBER : number;
        try {
            return new Page<>(newsDAO.findComments(newsId, from, number),
                    from,
                    number,
                    commentService.countComments(newsId));
        } catch (DAOException e) {
            throw new ServiceException(e);
        }
    }
}
