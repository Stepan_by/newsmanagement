package com.epam.newsmanagement.service.impl;

import com.epam.newsmanagement.dao.CommentDAO;
import com.epam.newsmanagement.dao.NewsDAO;
import com.epam.newsmanagement.domain.Comment;
import com.epam.newsmanagement.domain.News;
import com.epam.newsmanagement.dao.exception.DAOException;
import com.epam.newsmanagement.service.exception.ServiceException;
import com.epam.newsmanagement.service.CommentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.sql.Date;
import java.util.Calendar;

@Service
@Transactional
public class CommentServiceImpl implements CommentService {

    @Autowired
    private CommentDAO commentDAO;
    @Autowired
    private NewsDAO newsDAO;

    @Override
    public Comment add(Comment comment) throws ServiceException {
        try {
            comment.setCreationDate(new Date(Calendar.getInstance().getTimeInMillis()));
            return commentDAO.add(comment);
        } catch (DAOException e) {
            throw new ServiceException(e);
        }
    }

    @Override
    public Comment add(Long newsId, Comment comment) throws ServiceException {
        try {
            News news = newsDAO.find(newsId);
            comment.setNews(news);
            return add(comment);
        } catch (DAOException e) {
            throw new ServiceException(e);
        }
    }

    @Override
    public Comment find(Long commentId) throws ServiceException {
        try {
            return commentDAO.find(commentId);
        } catch (DAOException e) {
            throw new ServiceException(e);
        }
    }

    @Override
    public Comment update(Comment comment) throws ServiceException {
        try {
            return commentDAO.update(comment);
        } catch (DAOException e) {
            throw new ServiceException(e);
        }
    }

    @Override
    public Comment delete(Long commentId) throws ServiceException {
        try {
            return commentDAO.delete(commentId);
        } catch (DAOException e) {
            throw new ServiceException(e);
        }
    }

    @Override
    public Long countComments(Long newsId) throws ServiceException {
        try {
            return commentDAO.countComments(newsId);
        } catch (DAOException e) {
            throw new ServiceException(e);
        }
    }
}
