package com.epam.newsmanagement.dto;

import java.util.Collections;
import java.util.List;

public class SearchCriteria {
    private Long from;
    private Long number;
    private List<String> tags;
    private List<Long> authors;

    public SearchCriteria() {
    }

    public SearchCriteria(Long from, Long number, List<String> tags, List<Long> authors) {
        this.from = from;
        this.number = number;
        this.tags = tags;
        this.authors = authors;
    }

    public Long getFrom() {
        return from;
    }

    public void setFrom(Long from) {
        this.from = from;
    }

    public Long getNumber() {
        return number;
    }

    public void setNumber(Long number) {
        this.number = number;
    }

    public List<String> getTags() {
        return tags;
    }

    public void setTags(List<String> tags) {
        this.tags = tags;
    }

    public List<Long> getAuthors() {
        return authors;
    }

    public void setAuthors(List<Long> authors) {
        this.authors = authors;
    }

    public List<String> getTagsNames() {
        return tags == null ? Collections.emptyList() : tags;
    }

    public List<Long> getAuthorsIds() {
        return authors == null ? Collections.emptyList() : authors;
    }
}
