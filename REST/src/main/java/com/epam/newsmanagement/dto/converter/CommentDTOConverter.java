package com.epam.newsmanagement.dto.converter;


import com.epam.newsmanagement.domain.Comment;
import com.epam.newsmanagement.dto.CommentDTO;
import org.springframework.stereotype.Component;

@Component
public class CommentDTOConverter implements Converterable<Comment, CommentDTO> {

    @Override
    public CommentDTO convertToDTO(Comment entity) {
        return new CommentDTO(entity.getId(), entity.getText(), entity.getCreationDate());
    }

    @Override
    public Comment convertToEntity(CommentDTO dto) {
        return new Comment(dto.getId(), dto.getText(), dto.getCreationDate());
    }
}
