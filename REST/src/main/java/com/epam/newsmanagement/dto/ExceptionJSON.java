package com.epam.newsmanagement.dto;

import java.io.Serializable;

public class ExceptionJSON implements Serializable {

    private String url;
    private String message;

    public ExceptionJSON(String url, String message) {
        this.url = url;
        this.message = message;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ExceptionJSON that = (ExceptionJSON) o;

        if (url != null ? !url.equals(that.url) : that.url != null) return false;
        return message != null ? message.equals(that.message) : that.message == null;

    }

    @Override
    public int hashCode() {
        int result = url != null ? url.hashCode() : 0;
        result = 31 * result + (message != null ? message.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "ExceptionJSON{" +
                "url='" + url + '\'' +
                ", message='" + message + '\'' +
                '}';
    }
}
