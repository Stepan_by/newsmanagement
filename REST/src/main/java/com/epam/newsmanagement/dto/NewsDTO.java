package com.epam.newsmanagement.dto;

import com.epam.newsmanagement.domain.Author;
import com.epam.newsmanagement.domain.Tag;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

public class NewsDTO implements Serializable {

    private Long id;
    @NotNull
    @Size(max = 30)
    private String title;
    @NotNull
    @Size(max = 100)
    private String shortText;
    @NotNull
    @Size(max = 2000)
    private String fullText;
    private Date creationDate;
    private Date modificationDate;
    private Set<Author> authors = new HashSet<>();
    private Set<Tag> tags = new HashSet<>();

    public NewsDTO() {
    }

    public NewsDTO(Long id, String title, String shortText, String fullText, Date creationDate, Date modificationDate) {
        this.id = id;
        this.title = title;
        this.shortText = shortText;
        this.fullText = fullText;
        this.creationDate = creationDate;
        this.modificationDate = modificationDate;
    }

    public NewsDTO(Long id, String title, String shortText, String fullText, Date creationDate, Date modificationDate, Set<Author> authors, Set<Tag> tags) {
        this.id = id;
        this.title = title;
        this.shortText = shortText;
        this.fullText = fullText;
        this.creationDate = creationDate;
        this.modificationDate = modificationDate;
        this.authors = authors;
        this.tags = tags;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getShortText() {
        return shortText;
    }

    public void setShortText(String shortText) {
        this.shortText = shortText;
    }

    public String getFullText() {
        return fullText;
    }

    public void setFullText(String fullText) {
        this.fullText = fullText;
    }

    public Date getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }

    public Date getModificationDate() {
        return modificationDate;
    }

    public void setModificationDate(Date modificationDate) {
        this.modificationDate = modificationDate;
    }

    public Set<Author> getAuthors() {
        return authors;
    }

    public void setAuthors(Set<Author> authors) {
        this.authors = authors;
    }

    public Set<Tag> getTags() {
        return tags;
    }

    public void setTags(Set<Tag> tags) {
        this.tags = tags;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        NewsDTO newsDTO = (NewsDTO) o;

        if (id != null ? !id.equals(newsDTO.id) : newsDTO.id != null) return false;
        if (title != null ? !title.equals(newsDTO.title) : newsDTO.title != null) return false;
        if (shortText != null ? !shortText.equals(newsDTO.shortText) : newsDTO.shortText != null) return false;
        if (fullText != null ? !fullText.equals(newsDTO.fullText) : newsDTO.fullText != null) return false;
        if (creationDate != null ? !creationDate.equals(newsDTO.creationDate) : newsDTO.creationDate != null)
            return false;
        if (modificationDate != null ? !modificationDate.equals(newsDTO.modificationDate) : newsDTO.modificationDate != null)
            return false;
        if (authors != null ? !authors.equals(newsDTO.authors) : newsDTO.authors != null) return false;
        return tags != null ? tags.equals(newsDTO.tags) : newsDTO.tags == null;

    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + (title != null ? title.hashCode() : 0);
        result = 31 * result + (shortText != null ? shortText.hashCode() : 0);
        result = 31 * result + (fullText != null ? fullText.hashCode() : 0);
        result = 31 * result + (creationDate != null ? creationDate.hashCode() : 0);
        result = 31 * result + (modificationDate != null ? modificationDate.hashCode() : 0);
        result = 31 * result + (authors != null ? authors.hashCode() : 0);
        result = 31 * result + (tags != null ? tags.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "NewsDTO{" +
                "id=" + id +
                ", title='" + title + '\'' +
                ", shortText='" + shortText + '\'' +
                ", fullText='" + fullText + '\'' +
                ", creationDate=" + creationDate +
                ", modificationDate=" + modificationDate +
                ", authors=" + authors +
                ", tags=" + tags +
                '}';
    }
}
