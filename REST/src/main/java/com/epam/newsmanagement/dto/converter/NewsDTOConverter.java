package com.epam.newsmanagement.dto.converter;

import com.epam.newsmanagement.domain.News;
import com.epam.newsmanagement.dto.NewsDTO;
import org.springframework.stereotype.Component;

@Component
public class NewsDTOConverter implements Converterable<News, NewsDTO> {

    @Override
    public NewsDTO convertToDTO(News entity) {
        return new NewsDTO(
                entity.getId(),
                entity.getTitle(),
                entity.getShortText(),
                entity.getFullText(),
                entity.getCreationDate(),
                entity.getModificationDate(),
                entity.getAuthors(),
                entity.getTags());
    }

    @Override
    public News convertToEntity(NewsDTO dto) {
        return new News(
                dto.getId(),
                dto.getTitle(),
                dto.getShortText(),
                dto.getFullText(),
                dto.getCreationDate(),
                dto.getModificationDate(),
                dto.getAuthors(),
                dto.getTags());
    }
}
