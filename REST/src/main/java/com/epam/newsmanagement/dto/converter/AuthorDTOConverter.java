package com.epam.newsmanagement.dto.converter;

import com.epam.newsmanagement.domain.Author;
import com.epam.newsmanagement.dto.AuthorDTO;
import org.springframework.stereotype.Component;

@Component
public class AuthorDTOConverter implements Converterable<Author, AuthorDTO> {

    @Override
    public AuthorDTO convertToDTO(Author author) {
        return new AuthorDTO(author.getId(), author.getName());
    }

    @Override
    public Author convertToEntity(AuthorDTO authorDTO) {
        return new Author(authorDTO.getId(), authorDTO.getName());
    }
}
