package com.epam.newsmanagement.dto.converter;

public interface Converterable<A, B> {

    B convertToDTO(A entity);

    A convertToEntity(B dto);
}
