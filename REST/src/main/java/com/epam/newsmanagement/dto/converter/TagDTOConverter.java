package com.epam.newsmanagement.dto.converter;

import com.epam.newsmanagement.domain.Tag;
import com.epam.newsmanagement.dto.TagDTO;
import org.springframework.stereotype.Component;

@Component
public class TagDTOConverter implements Converterable<Tag, TagDTO> {

    @Override
    public TagDTO convertToDTO(Tag entity) {
        return new TagDTO(entity.getName());
    }

    @Override
    public Tag convertToEntity(TagDTO dto) {
        return new Tag(dto.getName());
    }
}
