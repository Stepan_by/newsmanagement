package com.epam.newsmanagement.exception;

import com.epam.newsmanagement.controller.exception.RequestException;
import com.epam.newsmanagement.dto.ExceptionJSON;
import com.epam.newsmanagement.service.exception.ServiceException;
import org.apache.log4j.Logger;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.NoHandlerFoundException;

import javax.servlet.http.HttpServletRequest;

@ControllerAdvice
public class RESTExceptionHandler {

    private static final Logger LOG = Logger.getLogger(RESTExceptionHandler.class);

    @ExceptionHandler(ServiceException.class)
    public ResponseEntity<ExceptionJSON> handleServiceException(HttpServletRequest request, ServiceException ex) {
        LOG.error(ex.getMessage());
        return new ResponseEntity<>(new ExceptionJSON(request.getRequestURI(), ex.getMessage()), HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(RuntimeException.class)
    public ResponseEntity<ExceptionJSON> handleRuntimeException(HttpServletRequest request, RuntimeException e) {
        LOG.error(e.getMessage());
        return new ResponseEntity<>(new ExceptionJSON(request.getRequestURI(), e.getMessage()), HttpStatus.INTERNAL_SERVER_ERROR);
    }

    @ExceptionHandler(NoHandlerFoundException.class)
    public ResponseEntity<ExceptionJSON> handle404Exception(HttpServletRequest request, Exception e0) {
        LOG.error(e0.getMessage());
        return new ResponseEntity<>(new ExceptionJSON(request.getRequestURI(), e0.getMessage()), HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler(Exception.class)
    public ResponseEntity<ExceptionJSON> handleGeneralException (HttpServletRequest request, Exception ex) {
        LOG.error(ex.getMessage());
        return new ResponseEntity<>(new ExceptionJSON(request.getRequestURI(), ex.getMessage()), HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(RequestException.class)
    public ResponseEntity<ExceptionJSON> handleBadRequestException (HttpServletRequest request, RuntimeException e) {
        LOG.error(e.getMessage());
        return new ResponseEntity<>(new ExceptionJSON(request.getRequestURI(), e.getMessage()), HttpStatus.BAD_REQUEST);
    }
}
