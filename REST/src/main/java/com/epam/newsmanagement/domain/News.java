package com.epam.newsmanagement.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

@Entity
@SequenceGenerator(name = "id_sequence", sequenceName = "NEWS_SEQ1", allocationSize = 1)
public class News implements Serializable {

    @Id
    @GeneratedValue( strategy= GenerationType.SEQUENCE, generator = "id_sequence" )
    @Column(name = "NW_NEWS_ID_PK")
    private Long id;

    @Column(name = "NW_TITLE")
    private String title;

    @Column(name = "NW_SHORT_TEXT")
    private String shortText;

    @Column(name = "NW_FULL_TEXT")
    private String fullText;

    @Column(name = "NW_CREATION_DATE")
    private Date creationDate;

    @Column(name = "NW_MODIFICATION_DATE")
    private Date modificationDate;

    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(name = "NEWS_AUTHORS",
            joinColumns = {@JoinColumn(name = "NA_NEWS_ID_FK", nullable = false)},
            inverseJoinColumns = {@JoinColumn(name = "NA_AUTHOR_ID_FK", nullable = false)})
    private Set<Author> authors = new HashSet<>();

    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(name = "NEWS_TAGS",
            joinColumns = {@JoinColumn(name = "NT_NEWS_ID_FK", referencedColumnName = "NW_NEWS_ID_PK", nullable = false)},
            inverseJoinColumns = {@JoinColumn(name = "NT_TAG_NAME_FK", referencedColumnName = "TG_TAG_NAME", nullable = false)})
    private Set<Tag> tags = new HashSet<>();

    @JsonIgnore
    @OneToMany(mappedBy = "news")
    private Set<Comment> comments = new HashSet<>();

    public News() {
    }

    public News(Long id, String title, String shortText, String fullText, Date creationDate) {
        this.id = id;
        this.title = title;
        this.shortText = shortText;
        this.fullText = fullText;
        this.creationDate = creationDate;
    }

    public News(String title, String shortText, String fullText, Date creationDate) {
        this.title = title;
        this.shortText = shortText;
        this.fullText = fullText;
        this.creationDate = creationDate;
    }

    public News(Long id, String title, String shortText, String fullText, Date creationDate, Date modificationDate) {
        this.id = id;
        this.title = title;
        this.shortText = shortText;
        this.fullText = fullText;
        this.creationDate = creationDate;
        this.modificationDate = modificationDate;
    }

    public News(String title, String shortText, String fullText, Date creationDate, Date modificationDate) {
        this.title = title;
        this.shortText = shortText;
        this.fullText = fullText;
        this.creationDate = creationDate;
        this.modificationDate = modificationDate;
    }

    public News(String title, String shortText, String fullText) {
        this.title = title;
        this.shortText = shortText;
        this.fullText = fullText;
    }

    public News(Long id, String title, String shortText, String fullText, Date creationDate, Date modificationDate, Set<Author> authors, Set<Tag> tags) {
        this.id = id;
        this.title = title;
        this.shortText = shortText;
        this.fullText = fullText;
        this.creationDate = creationDate;
        this.modificationDate = modificationDate;
        this.authors = authors;
        this.tags = tags;
    }

    public News(Long id, String title, String shortText, String fullText, Date creationDate, Date modificationDate, Set<Author> authors, Set<Tag> tags, Set<Comment> comments) {
        this.id = id;
        this.title = title;
        this.shortText = shortText;
        this.fullText = fullText;
        this.creationDate = creationDate;
        this.modificationDate = modificationDate;
        this.authors = authors;
        this.tags = tags;
        this.comments = comments;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getShortText() {
        return shortText;
    }

    public void setShortText(String shortText) {
        this.shortText = shortText;
    }

    public String getFullText() {
        return fullText;
    }

    public void setFullText(String fullText) {
        this.fullText = fullText;
    }

    public Date getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }

    public Date getModificationDate() {
        return modificationDate;
    }

    public void setModificationDate(Date modificationDate) {
        this.modificationDate = modificationDate;
    }

    public Set<Author> getAuthors() {
        return authors;
    }

    public void setAuthors(Set<Author> authors) {
        this.authors = authors;
    }

    public Set<Tag> getTags() {
        return tags;
    }

    public void setTags(Set<Tag> tags) {
        this.tags = tags;
    }

    public Set<Comment> getComments() {
        return comments;
    }

    public void setComments(Set<Comment> comments) {
        this.comments = comments;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        News news = (News) o;

        if (!id.equals(news.id)) return false;
        if (!title.equals(news.title)) return false;
        if (!shortText.equals(news.shortText)) return false;
        if (!fullText.equals(news.fullText)) return false;
        if (!creationDate.equals(news.creationDate)) return false;
        return modificationDate.equals(news.modificationDate);

    }

    @Override
    public int hashCode() {
        int result = id.hashCode();
        result = 31 * result + title.hashCode();
        result = 31 * result + shortText.hashCode();
        result = 31 * result + fullText.hashCode();
        result = 31 * result + creationDate.hashCode();
        result = 31 * result + modificationDate.hashCode();
        return result;
    }

    @Override
    public String toString() {
        return "News{" +
                "id=" + id +
                ", title='" + title + '\'' +
                ", shortText='" + shortText + '\'' +
                ", fullText='" + fullText + '\'' +
                ", creationDate=" + creationDate +
                ", modificationDate=" + modificationDate +
                '}';
    }
}
