package com.epam.newsmanagement.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "AUTHORS")
@SequenceGenerator(name = "id_sequence", sequenceName = "AUTHORS_SEQ1", allocationSize = 1)
public class Author implements Serializable {

    @Id
    @GeneratedValue( strategy=GenerationType.SEQUENCE, generator = "id_sequence")
    @Column(name = "AU_AUTHOR_ID_PK")
    private Long id;

    @Column(name = "AU_AUTHOR_NAME")
    private String name;

    @JsonIgnore
    @ManyToMany(mappedBy = "authors")
    private Set<News> news = new HashSet<>();

    public Author() {
    }

    public Author(Long id, String name) {
        this.id = id;
        this.name = name;
    }

    public Author(String name) {
        this.name = name;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Set<News> getNews() {
        return news;
    }

    public void setNews(Set<News> news) {
        this.news = news;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Author author = (Author) o;

        if (id != null ? !id.equals(author.id) : author.id != null) return false;
        return name != null ? name.equals(author.name) : author.name == null;

    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + (name != null ? name.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "Author{" +
                "id=" + id +
                ", name='" + name + '\'' +
                '}' ;
    }
}
