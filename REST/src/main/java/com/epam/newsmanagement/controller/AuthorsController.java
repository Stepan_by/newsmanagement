package com.epam.newsmanagement.controller;

import com.epam.newsmanagement.controller.exception.RequestException;
import com.epam.newsmanagement.domain.Author;
import com.epam.newsmanagement.dto.AuthorDTO;
import com.epam.newsmanagement.dto.converter.Converterable;
import com.epam.newsmanagement.service.AuthorService;
import com.epam.newsmanagement.service.exception.ServiceException;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Collection;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/api/authors")
public class AuthorsController {

    private static final Logger LOG = Logger.getLogger(AuthorsController.class);

    @Autowired
    private AuthorService authorService;
    @Autowired
    private Converterable<Author, AuthorDTO> authorDTOConverter;

    @RequestMapping(method = RequestMethod.GET)
    public ResponseEntity<Collection<AuthorDTO>> getAuthors() throws ServiceException {
        LOG.info("GET all authors");
        return new ResponseEntity<>(authorService.findAllAuthors().stream().
                map(e -> authorDTOConverter.convertToDTO(e)).
                collect(Collectors.toList()), HttpStatus.OK);
    }

    @RequestMapping(method = RequestMethod.POST)
    public ResponseEntity<AuthorDTO> addAuthor(@Valid @RequestBody AuthorDTO authorDTO,
                                               BindingResult bindingResult) throws ServiceException {
        LOG.info("POST author to the system " + authorDTO);
        if (bindingResult.hasErrors()) {
            throw new RequestException("Invalid author");
        }
        Author author = authorDTOConverter.convertToEntity(authorDTO);
        return new ResponseEntity<>(authorDTOConverter.convertToDTO(authorService.add(author)), HttpStatus.OK);
    }

    @RequestMapping(value = "/{id}",method = RequestMethod.DELETE)
    public ResponseEntity<AuthorDTO> deleteAuthor(@PathVariable Long id) throws ServiceException {
        LOG.info("DELETE author from the system : author id = " + id);
        return new ResponseEntity<>(authorDTOConverter.convertToDTO(authorService.delete(id)), HttpStatus.OK);
    }
}
