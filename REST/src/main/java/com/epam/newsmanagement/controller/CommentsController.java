package com.epam.newsmanagement.controller;


import com.epam.newsmanagement.controller.exception.RequestException;
import com.epam.newsmanagement.domain.Comment;
import com.epam.newsmanagement.dto.CommentDTO;
import com.epam.newsmanagement.dto.converter.Converterable;
import com.epam.newsmanagement.service.CommentService;
import com.epam.newsmanagement.service.NewsService;
import com.epam.newsmanagement.service.exception.ServiceException;
import com.epam.newsmanagement.wrapper.Page;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Collection;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/api/news")
public class CommentsController {

    private static final Logger LOG = Logger.getLogger(CommentsController.class);

    @Autowired
    private CommentService commentService;
    @Autowired
    private NewsService newsService;
    @Autowired
    private Converterable<Comment, CommentDTO> commentConverter;

    @RequestMapping(value = "/{newsId}/comments", method = RequestMethod.POST)
    public ResponseEntity<CommentDTO> addComment(@PathVariable Long newsId,
                                                 @Valid @RequestBody CommentDTO commentDTO,
                                                 BindingResult bindingResult) throws ServiceException {
        LOG.info("POST comment to news : news it = " + newsId + ", comment = " + commentDTO);
        if (bindingResult.hasErrors()) {
            throw new RequestException("Comment is not valid");
        }
        Comment comment = commentConverter.convertToEntity(commentDTO);
        return new ResponseEntity<>(commentConverter.convertToDTO(commentService.add(newsId, comment)), HttpStatus.OK);
    }

    @RequestMapping(value = "/{newsId}/comments", method = RequestMethod.GET)
    public ResponseEntity<Collection<CommentDTO>> getComments(@PathVariable Long newsId,
                                                              @RequestParam(value = "from", required = false) Long from,
                                                              @RequestParam(value = "number", required = false) Long number) throws ServiceException {
        LOG.info(String.format("GET comments for newsId=%d,from=%d,number=%d", newsId, from, number));
        Page<Comment> commentPage = newsService.findCommentsAtPage(newsId, from, number);
        HttpHeaders headers = new HttpHeaders();
        headers.add("size", String.valueOf(commentPage.getSize()));
        return new ResponseEntity<>(commentPage.getCollection().stream().map(comment -> commentConverter.
                convertToDTO(comment)).
                collect(Collectors.toList()), headers, HttpStatus.OK);
    }

    @RequestMapping(value = "/{newsId}/comments/{commentId}", method = RequestMethod.DELETE)
    public ResponseEntity<CommentDTO> deleteComment(@PathVariable Long newsId,
                                                    @PathVariable Long commentId) throws ServiceException {
        LOG.info("DELETE comment id = " + commentId);
        return new ResponseEntity<>(commentConverter.convertToDTO(commentService.delete(commentId)), HttpStatus.OK);
    }
}
