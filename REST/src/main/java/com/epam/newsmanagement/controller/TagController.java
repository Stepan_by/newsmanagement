package com.epam.newsmanagement.controller;

import com.epam.newsmanagement.controller.exception.RequestException;
import com.epam.newsmanagement.domain.Tag;
import com.epam.newsmanagement.dto.TagDTO;
import com.epam.newsmanagement.dto.converter.Converterable;
import com.epam.newsmanagement.service.exception.ServiceException;
import com.epam.newsmanagement.service.TagService;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Collection;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/api/tags")
public class TagController {

    private static final Logger LOG = Logger.getLogger(TagController.class);

    @Autowired
    private TagService tagService;
    @Autowired
    private Converterable<Tag, TagDTO> tagConverter;

    @RequestMapping(method = RequestMethod.GET)
    public ResponseEntity<Collection<TagDTO>> getTags() throws ServiceException {
        LOG.info("GET all tags");
        return new ResponseEntity<>(tagService.findAllTags().stream().map(tag -> tagConverter.
                convertToDTO(tag)).
                collect(Collectors.toList()), HttpStatus.OK);
    }

    @RequestMapping(method = RequestMethod.POST)
    public ResponseEntity<TagDTO> addTag(@Valid @RequestBody TagDTO tagDTO,
                                         BindingResult bindingResult) throws ServiceException {
        LOG.info("POST tag = " + tagDTO);
        if (bindingResult.hasErrors()) {
            throw new RequestException("Tag is not valid");
        }
        Tag tag = tagConverter.convertToEntity(tagDTO);
        return new ResponseEntity<>(tagConverter.convertToDTO(tagService.add(tag)), HttpStatus.OK);
    }

    @RequestMapping(value = "/{name}",method = RequestMethod.DELETE)
    public ResponseEntity<TagDTO> deleteTag(@PathVariable String name) throws ServiceException {
        LOG.info("DELETE tag name = " + name);
        return new ResponseEntity<>(tagConverter.convertToDTO(tagService.delete(name)), HttpStatus.OK);
    }
}
