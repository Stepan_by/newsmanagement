package com.epam.newsmanagement.controller;

import com.epam.newsmanagement.controller.exception.RequestException;
import com.epam.newsmanagement.domain.News;
import com.epam.newsmanagement.dto.NewsDTO;
import com.epam.newsmanagement.dto.SearchCriteria;
import com.epam.newsmanagement.dto.converter.Converterable;
import com.epam.newsmanagement.service.NewsService;
import com.epam.newsmanagement.service.exception.ServiceException;
import com.epam.newsmanagement.wrapper.Page;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Collection;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/api/news")
public class NewsController {

    private static final Logger LOG = Logger.getLogger(NewsController.class);

    @Autowired
    private NewsService newsService;
    @Autowired
    private Converterable<News, NewsDTO> newsConverter;

    @RequestMapping(value = "/{newsId}", method = RequestMethod.GET)
    public ResponseEntity<NewsDTO> getNews(@PathVariable Long newsId) throws ServiceException {
        LOG.info("GET news id = " + newsId);
        return new ResponseEntity<>(newsConverter.convertToDTO(newsService.find(newsId)), HttpStatus.OK);
    }

    @RequestMapping(method = RequestMethod.GET)
    public ResponseEntity<Collection<NewsDTO>> getNewsByCriteria(SearchCriteria searchCriteria) throws ServiceException {
        LOG.info(String.format("GET news from=%d, number=%d, tags=%s, authors=%s",
                searchCriteria.getFrom(),
                searchCriteria.getNumber(),
                searchCriteria.getTags(),
                searchCriteria.getAuthors()));
        Page<News> page = newsService.findNewsAtPage(searchCriteria);
        HttpHeaders headers = new HttpHeaders();
        headers.add("size", String.valueOf(page.getSize()));
        return new ResponseEntity<>(page.getCollection().stream().map(news -> newsConverter.
                convertToDTO(news)).
                collect(Collectors.toList()), headers, HttpStatus.OK);
    }

    @RequestMapping(method = RequestMethod.POST)
    public ResponseEntity<NewsDTO> saveNews(@Valid @RequestBody NewsDTO newsDTO,
                                            BindingResult result) throws ServiceException {
        LOG.info("POST news = " + newsDTO);
        if (result.hasErrors()) {
            throw new RequestException("Invalid news");
        }
        News news = newsConverter.convertToEntity(newsDTO);
        return new ResponseEntity<>(newsConverter.convertToDTO(newsService.add(news)), HttpStatus.OK);
    }

    @RequestMapping(method = RequestMethod.PUT)
    public ResponseEntity<NewsDTO> updateNews(@Valid @RequestBody NewsDTO newsDTO,
                                              BindingResult result) throws ServiceException {
        LOG.info("PUT news " + newsDTO);
        if (result.hasErrors()) {
            throw new RequestException("Invalid news");
        }
        News news = newsConverter.convertToEntity(newsDTO);
        return new ResponseEntity<>(newsConverter.convertToDTO(newsService.update(news)), HttpStatus.OK);
    }

    @RequestMapping(value = "/{newsId}", method = RequestMethod.DELETE)
    public ResponseEntity<NewsDTO> deleteNews(@PathVariable Long newsId) throws ServiceException {
        LOG.info("DELETE news id = " + newsId);
        return new ResponseEntity<>(newsConverter.convertToDTO(newsService.delete(newsId)), HttpStatus.OK);
    }

    @RequestMapping(value = "/top{top}", method = RequestMethod.GET)
    public ResponseEntity<Collection<NewsDTO>> getTopNews(@RequestParam(value = "from", required = false) Long from,
                                                          @RequestParam(value = "number", required = false) Long number,
                                                          @PathVariable Long top) throws ServiceException {
        LOG.info("GET top news top = " + top );
        return new ResponseEntity<>(newsService.findTopNews(from, number, top).stream().map(news -> newsConverter.
                convertToDTO(news)).
                collect(Collectors.toList()), HttpStatus.OK);
    }
}
