package com.epam.newsmanagement.wrapper;

import java.io.Serializable;
import java.util.List;

public class Page<T> implements Serializable {

    private List<T> collection;
    private Long from;
    private Long number;
    private Long size;

    public Page() {
    }

    public Page(List<T> collection, Long from, Long number, Long size) {
        this.collection = collection;
        this.from = from;
        this.number = number;
        this.size = size;
    }

    public List<T> getCollection() {
        return collection;
    }

    public void setCollection(List<T> collection) {
        this.collection = collection;
    }

    public Long getFrom() {
        return from;
    }

    public void setFrom(Long from) {
        this.from = from;
    }

    public Long getNumber() {
        return number;
    }

    public void setNumber(Long number) {
        this.number = number;
    }

    public Long getSize() {
        return size;
    }

    public void setSize(Long size) {
        this.size = size;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Page<?> page = (Page<?>) o;

        if (collection != null ? !collection.equals(page.collection) : page.collection != null) return false;
        if (from != null ? !from.equals(page.from) : page.from != null) return false;
        if (number != null ? !number.equals(page.number) : page.number != null) return false;
        return size != null ? size.equals(page.size) : page.size == null;

    }

    @Override
    public int hashCode() {
        int result = collection != null ? collection.hashCode() : 0;
        result = 31 * result + (from != null ? from.hashCode() : 0);
        result = 31 * result + (number != null ? number.hashCode() : 0);
        result = 31 * result + (size != null ? size.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "Page{" +
                "collection=" + collection +
                ", from=" + from +
                ", number=" + number +
                ", size=" + size +
                '}';
    }
}
