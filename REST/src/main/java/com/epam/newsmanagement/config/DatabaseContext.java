package com.epam.newsmanagement.config;

import org.apache.commons.dbcp2.BasicDataSource;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.dao.annotation.PersistenceExceptionTranslationPostProcessor;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import javax.annotation.Resource;
import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;
import java.util.Properties;

@Configuration
@PropertySource("classpath:db.properties")
@EnableTransactionManagement
public class DatabaseContext {

    @Resource
    private Environment env;
    @Value("hibernate.properties")
    private Properties properties;

    @Bean
    public PlatformTransactionManager transactionManager(EntityManagerFactory entityManagerFactory) {
        return new JpaTransactionManager(entityManagerFactory);
    }

    @Bean(name = "dataSource")
    public DataSource dataSource() {
        BasicDataSource ds = new BasicDataSource();
        ds.setUrl(env.getRequiredProperty("db_url"));
        ds.setDriverClassName(env.getRequiredProperty("db_driver_class"));
        ds.setUsername(env.getRequiredProperty("db_username"));
        ds.setPassword(env.getRequiredProperty("db_password"));

        ds.setInitialSize(Integer.valueOf(env.getRequiredProperty("cp.initialSize")));
        ds.setMinIdle(Integer.valueOf(env.getRequiredProperty("cp.minIdle")));
        ds.setMaxIdle(Integer.valueOf(env.getRequiredProperty("cp.maxIdle")));
        ds.setTimeBetweenEvictionRunsMillis(Long.valueOf(env.getRequiredProperty("cp.timeBetweenEvictionRunsMillis")));
        ds.setMinEvictableIdleTimeMillis(Long.valueOf(env.getRequiredProperty("cp.minEvictableIdleTimeMillis")));
        ds.setTestOnBorrow(Boolean.valueOf(env.getRequiredProperty("cp.testOnBorrow")));
        ds.setValidationQuery(env.getRequiredProperty("cp.validationQuery"));

        return ds;
    }

    @Bean
    public LocalContainerEntityManagerFactoryBean localEntityManagerFactoryBean() {
        LocalContainerEntityManagerFactoryBean localEntityManagerFactoryBean = new LocalContainerEntityManagerFactoryBean();
        localEntityManagerFactoryBean.setDataSource(dataSource());
        localEntityManagerFactoryBean.setJpaProperties(properties);
        localEntityManagerFactoryBean.setPackagesToScan(env.getRequiredProperty("db_package_to_scan"));
        localEntityManagerFactoryBean.setJpaVendorAdapter(new HibernateJpaVendorAdapter());
        return localEntityManagerFactoryBean;
    }

    @Bean
    public PersistenceExceptionTranslationPostProcessor exceptionTranslationPostProcessor () {
        return new PersistenceExceptionTranslationPostProcessor();
    }
}
